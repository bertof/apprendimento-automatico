# Appunti Apprendimento automatico

Riassunto del corso di apprendimento automatico.

Fonti: [Slide Prof Aiolli](https://www.math.unipd.it/~aiolli) e T. Mitchell , "Machine Learning", McGraw Hill, 1998.

Autore: Filippo Berto

[Versione PDF](apprendimento-automatico.pdf)

## Indice

1. [Indice](#Indice)
1. [Introduzione](#Introduzione)
   1. [Definizione problema ben posto](#Definizione-problema-ben-posto)
   1. [Deduzione, Induzione e Abduzione](#Deduzione-Induzione-e-Abduzione)
   1. [Quando usare machine learning](#Quando-usare-machine-learning)
   1. [Quando è importante l'apprendimento](#Quando-è-importante-lapprendimento)
   1. [Dati vs Conoscenza](#Dati-vs-Conoscenza)
   1. [Assunzione fondamentale](#Assunzione-fondamentale)
1. [Riassunto basi matematiche](#Riassunto-basi-matematiche)
   1. [Probabilità](#Probabilità)
   1. [Media](#Media)
   1. [Varianza](#Varianza)
   1. [Distribuzioni notevoli](#Distribuzioni-notevoli)
      1. [Bernoulli](#Bernoulli)
      1. [Binomiale](#Binomiale)
      1. [Uniforme in $[a,b]$](#Uniforme-in-ab)
      1. [Normale (Gaussiana)](#Normale-Gaussiana)
   1. [Vettori](#Vettori)
      1. [Prodotto scalare](#Prodotto-scalare)
      1. [Lunghezza di un vettore](#Lunghezza-di-un-vettore)
      1. [Proiezione di vettori](#Proiezione-di-vettori)
   1. [Matrici](#Matrici)
      1. [Addizione](#Addizione)
      1. [Prodotto](#Prodotto)
      1. [Trasposta](#Trasposta)
      1. [Inversa](#Inversa)
      1. [Pseudo-inversa](#Pseudo-inversa)
      1. [Matrice definita positiva](#Matrice-definita-positiva)
   1. [Derivate](#Derivate)
   1. [Gradiente](#Gradiente)
   1. [Minimizzazione con discesa di gradiente](#Minimizzazione-con-discesa-di-gradiente)
   1. [Algoritmo di discesa di gradiente](#Algoritmo-di-discesa-di-gradiente)
1. [Supervised learning](#Supervised-learning)
   1. [Componenti dell'apprendimento](#Componenti-dellapprendimento)
      1. [Errore empirico](#Errore-empirico)
      1. [Errore ideale](#Errore-ideale)
   1. [Learning setting](#Learning-setting)
   1. [Bias induttivo](#Bias-induttivo)
   1. [Shattering](#Shattering)
   1. [VC-dimension](#VC-dimension)
   1. [Errore di generalizzazione](#Errore-di-generalizzazione)
   1. [Structural Risk Minimization](#Structural-Risk-Minimization)
1. [Concept learning](#Concept-learning)
   1. [Definizioni in Concept Learning](#Definizioni-in-Concept-Learning)
      1. [Concetto e consistenza](#Concetto-e-consistenza)
      1. [Struttura dello spazio delle ipotesi](#Struttura-dello-spazio-delle-ipotesi)
   1. [Algoritmo FIND-S](#Algoritmo-FIND-S)
      1. [Osservazioni](#Osservazioni)
      1. [Limiti di FIND-S](#Limiti-di-FIND-S)
         1. [Convergenza al concetto target](#Convergenza-al-concetto-target)
      1. [Ipotesi più specifica](#Ipotesi-più-specifica)
      1. [Consistenza degli esempi](#Consistenza-degli-esempi)
      1. [Unicità della soluzione](#Unicità-della-soluzione)
   1. [Candidate-Elimination](#Candidate-Elimination)
      1. [Definizioni in Candidate-Elimination](#Definizioni-in-Candidate-Elimination)
1. [Alberi di decisione](#Alberi-di-decisione)
   1. [Definizioni in Alberi di decisione](#Definizioni-in-Alberi-di-decisione)
   1. [Quando usare gli Alberi di decisione](#Quando-usare-gli-Alberi-di-decisione)
   1. [Alberi di decisione e funzioni booleane](#Alberi-di-decisione-e-funzioni-booleane)
   1. [Algoritmo ID3](#Algoritmo-ID3)
   1. [Scelta dell'attributo ottimo in ID3](#Scelta-dellattributo-ottimo-in-ID3)
      1. [Entropia](#Entropia)
      1. [Information Gain](#Information-Gain)
      1. [Criteri alternativi per la scelta dell'attributo ottimo](#Criteri-alternativi-per-la-scelta-dellattributo-ottimo)
   1. [Controindicazioni sull'uso dell'Information Gain](#Controindicazioni-sulluso-dellInformation-Gain)
   1. [Bias induttivo negli Alberi di Decisione](#Bias-induttivo-negli-Alberi-di-Decisione)
   1. [Attributi continui](#Attributi-continui)
   1. [Attributi con valori mancanti](#Attributi-con-valori-mancanti)
   1. [Overfitting negli alberi di decisione](#Overfitting-negli-alberi-di-decisione)
      1. [Reduced Error Pruning](#Reduced-Error-Pruning)
      1. [Rule-Post Pruning](#Rule-Post-Pruning)
      1. [Considerazioni su Rule-Post Pruning](#Considerazioni-su-Rule-Post-Pruning)
1. [Reti neurali](#Reti-neurali)
   1. [Quando usare RNA](#Quando-usare-RNA)
   1. [Il Perceptron](#Il-Perceptron)
   1. [Funzioni booleane e Perceptron](#Funzioni-booleane-e-Perceptron)
   1. [Algoritmo di apprendimento del Perceptron](#Algoritmo-di-apprendimento-del-Perceptron)
   1. [Osservazioni sul l'algoritmo del perceptron](#Osservazioni-sul-lalgoritmo-del-perceptron)
   1. [Componenti del Perceptron](#Componenti-del-Perceptron)
   1. [Reti Neurali Multistrato](#Reti-Neurali-Multistrato)
   1. [Regola Delta per Perceptron](#Regola-Delta-per-Perceptron)
   1. [Algoritmo con discesa di gradiente](#Algoritmo-con-discesa-di-gradiente)
   1. [Attivazione con sigmoide](#Attivazione-con-sigmoide)
   1. [Terminologia](#Terminologia)
   1. [Calcolo gradiente per i pesi di una unità di output](#Calcolo-gradiente-per-i-pesi-di-una-unità-di-output)
   1. [Calcolo gradiente per i pesi di una unità nascosta](#Calcolo-gradiente-per-i-pesi-di-una-unità-nascosta)
   1. [Discesa Batch e Stocastica](#Discesa-Batch-e-Stocastica)
   1. [Algoritmo di Backpropagation con uno strato nascosto](#Algoritmo-di-Backpropagation-con-uno-strato-nascosto)
   1. [Universalità delle reti multi-strato](#Universalità-delle-reti-multi-strato)
      1. [Teorema di Pinkus, 1996 (semplificato)](#Teorema-di-Pinkus-1996-semplificato)
   1. [Problemi nelle reti multi-strato](#Problemi-nelle-reti-multi-strato)
   1. [Evitare i minimi locali](#Evitare-i-minimi-locali)
   1. [Rappresentazione dei livelli nascosti](#Rappresentazione-dei-livelli-nascosti)
   1. [Overfitting](#Overfitting)
1. [Modelli lineari](#Modelli-lineari)
   1. [Emulare l'algoritmo del Perceptron con un modello lineare](#Emulare-lalgoritmo-del-Perceptron-con-un-modello-lineare)
   1. [Regressione Lineare Multivariata](#Regressione-Lineare-Multivariata)
   1. [Generalized Linear Models](#Generalized-Linear-Models)
   1. [Margine di un iperpiano](#Margine-di-un-iperpiano)
   1. [Support Vector Machines](#Support-Vector-Machines)
      1. [Teorema Support Vector Machines](#Teorema-Support-Vector-Machines)
   1. [SVM nel caso separabile](#SVM-nel-caso-separabile)
      1. [Soluzione del problema di ottimizzazione nel caso separabile](#Soluzione-del-problema-di-ottimizzazione-nel-caso-separabile)
   1. [SVM nel caso non separabile](#SVM-nel-caso-non-separabile)
   1. [SVM con trasformazioni non lineari](#SVM-con-trasformazioni-non-lineari)
   1. [Funzioni Kernel](#Funzioni-Kernel)
      1. [Esempi di funzioni Kernel](#Esempi-di-funzioni-Kernel)
   1. [SVM con funzioni Kernel](#SVM-con-funzioni-Kernel)
   1. [Regressione con SVM](#Regressione-con-SVM)
1. [Problemi pratici nell'apprendimento](#Problemi-pratici-nellapprendimento)
   1. [Bias e Varianza](#Bias-e-Varianza)
   1. [Underfitting e Overfitting](#Underfitting-e-Overfitting)
   1. [Model selection](#Model-selection)
      1. [Hold-out](#Hold-out)
      1. [K-fold Cross Validation](#K-fold-Cross-Validation)
   1. [Valutazione di dati sbilanciati](#Valutazione-di-dati-sbilanciati)
      1. [Tavola di Contingenza](#Tavola-di-Contingenza)
      1. [F-Measure](#F-Measure)
   1. [Da classificazione binaria a classificazione multiclasse](#Da-classificazione-binaria-a-classificazione-multiclasse)
      1. [One vs Rest](#One-vs-Rest)
      1. [One vs One](#One-vs-One)
   1. [Micro e Macro Averaging](#Micro-e-Macro-Averaging)
      1. [Macro Averaging](#Macro-Averaging)
      1. [Micro Averaging](#Micro-Averaging)
1. [Preprocessing](#Preprocessing)
   1. [Feature categoriche o simboliche](#Feature-categoriche-o-simboliche)
   1. [Feature quantitative o numeriche](#Feature-quantitative-o-numeriche)
   1. [One-Hot Encoding](#One-Hot-Encoding)
   1. [Codifica delle variabili continue](#Codifica-delle-variabili-continue)
   1. [Preprocessing per variabili continue](#Preprocessing-per-variabili-continue)
1. [Metodi Bayesiani](#Metodi-Bayesiani)
   1. [Teorema di Bayes](#Teorema-di-Bayes)
   1. [Scelta dell'ipotesi](#Scelta-dellipotesi)
   1. [Apprendimento brute force dell'ipotesi MAP](#Apprendimento-brute-force-dellipotesi-MAP)
   1. [Classificazione ottima di Bayes](#Classificazione-ottima-di-Bayes)
      1. [Algoritmo di Gibbs](#Algoritmo-di-Gibbs)
   1. [Classificatore Naive Bayes](#Classificatore-Naive-Bayes)
      1. [Algoritmo Naive Bayes](#Algoritmo-Naive-Bayes)
      1. [Considerazioni aggiuntive Naive Bayes](#Considerazioni-aggiuntive-Naive-Bayes)
   1. [Algoritmo Expected Maximization](#Algoritmo-Expected-Maximization)
      1. [Funzionamento EM](#Funzionamento-EM)
      1. [EM per stimare le k-medie](#EM-per-stimare-le-k-medie)
1. [Kernel e rappresentazione](#Kernel-e-rappresentazione)
   1. [Rappresentazione con kernel](#Rappresentazione-con-kernel)
      1. [Matrici Kernel e di Gram](#Matrici-Kernel-e-di-Gram)
   1. [Vantaggi dell'uso di kernel](#Vantaggi-delluso-di-kernel)
   1. [Modularità dei kernel](#Modularità-dei-kernel)
   1. [Kernel Trick](#Kernel-Trick)
   1. [Tipici kernel per vettori](#Tipici-kernel-per-vettori)
   1. [Proprietà di chiusura](#Proprietà-di-chiusura)
   1. [Estensione dei kernel ad altri tipi di strutture](#Estensione-dei-kernel-ad-altri-tipi-di-strutture)
   1. [Kernel Booleani](#Kernel-Booleani)
      1. [Kernel congiuntivo](#Kernel-congiuntivo)
      1. [Kernel disgiuntivo](#Kernel-disgiuntivo)
   1. [Kernel in forma normale](#Kernel-in-forma-normale)
   1. [Apprendimento per kernel](#Apprendimento-per-kernel)
   1. [Metodi parametrici](#Metodi-parametrici)
   1. [Estrazioni di feature tramite kernel non lineari](#Estrazioni-di-feature-tramite-kernel-non-lineari)
   1. [Apprendimento di kernel spettrali](#Apprendimento-di-kernel-spettrali)
1. [Clustering](#Clustering)
   1. [Scelte da fare nel Clustering](#Scelte-da-fare-nel-Clustering)
   1. [Funzione obbiettivo](#Funzione-obbiettivo)
   1. [Valutazione di un clustering](#Valutazione-di-un-clustering)
      1. [Metodi interni](#Metodi-interni)
      1. [Metodi esterni](#Metodi-esterni)
   1. [Algoritmi di clustering](#Algoritmi-di-clustering)
   1. [Algoritmo K-Means](#Algoritmo-K-Means)
   1. [Algoritmi gerarchici](#Algoritmi-gerarchici)
      1. [Clustering Gerarchico Agglomerativo (HAC)](#Clustering-Gerarchico-Agglomerativo-HAC)
1. [Sistemi di raccomandazione](#Sistemi-di-raccomandazione)
   1. [Tipologie di feedback in RS](#Tipologie-di-feedback-in-RS)
   1. [Approcci in RS](#Approcci-in-RS)
   1. [Matrice di rating per feedback esplicito](#Matrice-di-rating-per-feedback-esplicito)
   1. [Problemi in RS](#Problemi-in-RS)
   1. [Valutazione del rating](#Valutazione-del-rating)
      1. [Rates Prediction (Root Mean Squared Error)](#Rates-Prediction-Root-Mean-Squared-Error)
      1. [Top-N Reccomendation](#Top-N-Reccomendation)
   1. [Collaborative Filtering](#Collaborative-Filtering)
      1. [Matrix Factorizzation e Regressione](#Matrix-Factorizzation-e-Regressione)
      1. [Nearest Neighbour Based](#Nearest-Neighbour-Based)

## Introduzione

### Definizione problema ben posto

Un programma impara dall'esperienza $E$ rispetto ad una classe di task $T$ e le sue performance vengono misurate tramite $P$. Le performance nei task in $T$, misurati secondo $P$, migliorano all'aumentare di $E$.

### Deduzione, Induzione e Abduzione

- Deduzione: $Regola+Caso\rightarrow Risultato$
- Induzione: $Caso+Risultato\rightarrow Regola$
- Abduzione: $Regola+Risultato\rightarrow Caso$

### Quando usare machine learning

- Impossibilità di formalizzare il problema
- Rumore e/o incertezza in input/output
- Alta complessità della soluzione
- Inefficienza della soluzione
- Mancanza di conoscenza sul problema da risolvere

### Quando è importante l'apprendimento

- Necessità di adattarsi all'ambiente
- Necessità di migliorare le prestazioni
- Necessità di scoprire regolarità e nuova informazione

### Dati vs Conoscenza

- I dati sono informazione empirica, non necessariamente corretta
- La conoscenza è un insieme di statement sicuramente veri

### Assunzione fondamentale

Esiste un processo (stocastico) che spiega i dati che osserviamo.
Magari non conosciamo i dettagli su come funziona, ma esiste.

Lo scopo di ML è costruire buone/utili approssimazioni di questo processo.

## Riassunto basi matematiche

### Probabilità

- $P(E\bigcup F)=P(E)+P(F)-P(E\bigcap F)$
- $P(E\bigcap F)=P(E\mid F){P(F)}$ (Formula di Bayes)
- $E$ e $F$ sono indipendenti se $P(E\mid F)=P(E)$

### Media

- $E[aX+b]=aE[x]+b$
- $E[X+Y]=E[X]+E[Y]$
- $E[x^n]=\sum_{i}x_i^n P(x_i)$

### Varianza

- $Var(X)=E[(X-\mu)^2]=E[x^2]-\mu^2$
- $\sigma(x)=\sqrt{Var(x)}$

### Distribuzioni notevoli

#### Bernoulli

$P(X=1)=p$ e $P(X=0)=1-p$, allora:
$$E[X]=p$$
$$Var(X)=p(1-p)$$

#### Binomiale

$P(X=1)=p$ e $P(X=0)=1-p$, allora:
$$P(X=i)=\binom{N}{i}p^i(1-p)^{N-i}$$

#### Uniforme in $[a,b]$

$a\leq x\leq b$, allora:
$$p(x)=\frac{1}{b-a}$$

#### Normale (Gaussiana)

Media $\mu$ e varianza $\sigma^2$, allora:
$$p(x)=\frac{1}{\sqrt{2\pi}\sigma}e^{1-\frac{(x-\mu)^2}{2\sigma^2}}$$

### Vettori

#### Prodotto scalare

$$
\mathbf{x}\cdot\mathbf{z}=\sum_{i}x_i z_i = |\mathbf{x}||\mathbf{z}|cos(\Theta)
$$

#### Lunghezza di un vettore

$$
|x|^2=\sum_ix_i^2
$$

#### Proiezione di vettori

Proiezione del vettore $\mathbf{x}$ lungo il vettore $\mathbf{z}$:
$$
\mathbf{x_z}=(\frac{\mathbf{x}\cdot\mathbf{z}}{\mathbf{z}\cdot\mathbf{z}})=\alpha z
$$
Un'interpretazione probabilistica è $P(x\mid z)=\alpha$

### Matrici

#### Addizione

$$
[\mathbf{A}+\mathbf{B}]_{ij}=[\mathbf{A}]_{i,j}+[\mathbf{B}]_{ij}=a_{ij}+b_{ij}
$$

#### Prodotto

$$
[AB]_{ij}=\sum_{q=1}^k [\mathbf{A}]_{iq}[\mathbf{B}]_{qj}=\sum_{q=1}^k a_{iq}b_{qj}
$$

#### Trasposta

$$
[\mathbf{A}^T]_{ij}=\mathbf{A}_{ji}
$$
$$
(\mathbf{A}^T)^T=\mathbf{A}
$$
$$
(\mathbf{AB})^T=\mathbf{B}^T\mathbf{A}^T
$$
$$
\mathbf{A}=\mathbf{A}^T\implies A\text{ simmetrica}
$$

#### Inversa

$$\mathbf{A}\mathbf{A}^{-1}=\mathbf{I}=\mathbf{A}^{-1}\mathbf{A}$$

È possibile trovare la matrice inversa solo se il rango è massimo e il determinante diverso da $0$.

#### Pseudo-inversa

Per matrici rettangolari, se la matrice quadrata $\mathbf{A}\mathbf{A}^T$ è invertibile, allora la matrice pseudo-inversa $\mathbf{A}^\dagger=\mathbf{A}^T(\mathbf{A}\mathbf{A}^T)^{-1}$ soddisfa $\mathbf{A}\mathbf{A}^\dagger=\mathbf{I}$.

#### Matrice definita positiva

Una matrice simmetrica $\mathbf{A}\in\mathcal{R}^{n\times n}$ con la proprietà che $\mathbf{x}^T\mathbf{A}\mathbf{x}\geq 0$ per ogni vettore $\mathbf{x}\in\mathcal{R}^n$ si dice semi-definita positiva.

Una matrice simmetrica $\mathbf{A}\in\mathcal{R}^{n\times n}$ con la proprietà che $\mathbf{x}^T\mathbf{A}\mathbf{x}>0$ per ogni vettore $\mathbf{x}\in\mathcal{R}^n$ si dice definita positiva.

Una matrice definita positiva è sempre invertibile.

### Derivate

- $y=c,c\in\mathbb{R}\implies y'=0$
- $y=c\cdot x,c\in\mathbb{R}\implies y'=c$
- $y=x^n,n\in\mathbb{N}\implies y'=nx^{n-1}$
- $y=e^x\implies y'=e^x$
- $D(k\cdot f(x))=k\cdot f'(x)$
- $D(\sum:if_i(x))=\sum_if'(x)$
- $D(f(x)\cdot g(x))=f'(x)\cdot g(x)+f(x)\cdot g'(x)$
- $D(\frac{1}{g(x)})=-\frac{g'(x)}{g(x)^2}$
- $D(g(f(x)))=g'(f(x))\cdot f'(x)$

### Gradiente

Nel calcolo differenziale vettoriale, il gradiente di una funzione a valori reali $f:\mathbb{R}^n\rightarrow\mathbb{R}$ è una funzione vettoriale $\nabla :\mathbb{R}^n\rightarrow\mathbb{R}^n$ a valori in $\mathbb{R}^n$. Il gradiente di una funzione è spesso definito come il vettore che ha come componenti le derivate parziali della funzione, anche se questo vale solo se si utilizzano coordinate cartesiane ortonormali. $\nabla$ si legge **nabla**.

### Minimizzazione con discesa di gradiente

Sia $f(\mathbf{x}:\mathbb{R}^n\rightarrow\mathbb{R})$ una funzione a valori vettoriali, trovare il vettore $\mathbf{x}*\in\mathbb{R}^n$ che minimizza la funzione $f$, ovvero $\mathbf{x}^*=\text{arg min}_\mathbf{x}f(\mathbf{x})$

### Algoritmo di discesa di gradiente

- $h\leftarrow 0,\mathbf{x}_0\in\mathbb{R}^n$
- **while** $\nabla f(\mathbf{x}_k)\neq0$
  - Calcolare la direzione di discesa $\mathbf{p}_k:=-\nabla f(\mathbf{x}_k)$
  - Calcolare il passo di discesa $\eta_k$
  - $\mathbf{x}_{k+1}\leftarrow\mathbf{x}_k+\eta_k\mathbf{p}_k$
  - $k\leftarrow k+1$

Non è detto che l'algoritmo converga ad un minimo globale.

## Supervised learning

### Componenti dell'apprendimento

- Input: $x\in\mathcal{X}$
- Output: $y\in\mathcal{Y}$
- Oracolo:
  - Funzione target $f:\mathcal{X}\rightarrow\mathcal{Y}$ determinista
  - Distribuzioni di probabilità $P(\mathbf{x})$ e $P(y\mid\mathbf{x})$
- Dati: $\{(\mathbf{x}_1,y_1),\ldots,(\mathbf{x}_n,y_n)\}$ generati dalla distribuzione di probabilità $P(\mathbf{x},y)=P(\mathbf{x})P(y\mid\mathbf{x})$
- Funzione ipotesi: $g:\mathcal{X}\rightarrow\mathcal{Y}$ da un insieme $\mathcal{H}$

#### Errore empirico

L'errore/rischio empirico è l'errore sui dati di training, ovvero quanto $g$ sbaglia sui dati che abbiamo

#### Errore ideale

l'Errore/rischio ideale è l'errore atteso ottenuto da un'estrazione dalla distribuzione $P(\mathbf{x},y)$ di una coppia $(\mathbf{x},y)$.

### Learning setting

```none
   Target function
       f:X->y
         |
   Training examples
   (x1,y1),(x2,y2)...
         |        Learning algorithm      Final Hypothesis
         -------->        A         ----->      G~=f
         |
   Hypothesis set
         H
```

### Bias induttivo

Affinché l'apprendimento sia fattibile devono essere fatte alcune assunzioni sulla complessità della funzione obbiettivo e dello spazio delle ipotesi.
Lo spazio delle ipotesi non può contenere tutte le possibili formule/funzioni e le assunzioni che facciamo per approssimare la funzione obbiettivo vengono dette bias induttivo.
In particolare consistono in:

- Scelta dello spazio delle ipotesi $\mathcal{H}$
- Scelta dell'algoritmo di apprendimento $A$, ovvero come $\mathcal{H}$ viene esplorato.

### Shattering

Dato $S\subset X$, $S$ è shattered dallo spazio delle ipotesi $\mathcal{H}$ sse $\forall S'\subseteq S,\exists h\in\mathcal{H}.\forall x\in S.h(x)=1\iff x\in S'$.

Possiamo anche dire che $\mathcal{H}$ può implementare tutte le possibili dicotomie di $S$.

### VC-dimension

La VC-dimension di uno spazio delle ipotesi $\mathcal{H}$ definito su un instance space $X$ è la dimensione del più grande sottoinsieme finito di $X$ che è shattered da $\mathcal{H}$:

$$
VC(\mathcal{H})=\underset{S\subseteq}{\text{max}}|S|: S\text{ is shattered by }\mathcal{H}
$$

Ad esempio la VC-dimension di una retta su un piano è 3, poiché è il massimo numero di punti di cui è possibile dare una dicotomia completa, ovvero mostrare un esempio per tutti i modi cui i punti possono essere ripartiti.

### Errore di generalizzazione

Consideriamo un problema di classificazione binaria:

- Training set $S=\{(\mathbf{x}_1,y_1),\ldots,(\mathbf{x}_n,y_n)\}$
- Spazio delle ipotesi $\mathcal{H}=\{h_\Theta(\mathbf{x})\}$
- Algoritmo di apprendimento $\mathcal{L}$ che restituisce un'ipotesi $g=h_\Theta^*$ che minimizza l'errore empirico su $\mathcal{S}$, ovvero $g=\text{arg min}_{h\in\mathcal{H}}error_S(h)$

È possibile derivare un limite superiore all'errore ideale, valido con probabilità $(1-\delta)$, con $\delta$ arbitrariamente piccola della forma:

$$
   error(g)\leq error_S(g)+F(\frac{VC(\mathcal{H})}{n},\delta)
$$

Definendo i due termini del bound:

- $A = error_S(g)$
- $B = F(VC(\mathcal{H})/n,\delta)$

Il termine $A$ dipende dall'ipotesi restituita dall'algoritmo di apprendimento $\mathcal{L}$.

Il termine $B$ (chiamato VC-confidence) non dipende da $\mathcal{L}$ ma solo da:

- La dimensione del training set
- La VC dimension dello spazio di ipotesi $VC(\mathcal{H})$
- La confidenza $\delta$

### Structural Risk Minimization

All'aumentare della VC-dimension, l'errore empirico (A) decresce ma aumenta la VC-confidence (B).
È possibile cercare induttivamente un punto che minimizzi l'errore dato da queste due componenti, in questo modo si cerca il migliore tradeoff tra i due.

## Concept learning

L'biettivo del concept learning è quello di definire una categoria/concetto basata su esempi positivi e negativi su di essa.
Si cerca di capire cosa appartiene o meno ad un insieme per esempi su cosa è al suo interno o meno.

Si dice istanza di Supervised Learning il problema della ricerca in uno spazio delle ipotesi potenziali quella che meglio si adatta/fitta gli esempi di training.
Per fare ciò sfrutteremo la forma/struttura dello spazio delle ipotesi per rendere la ricerca più efficiente.

### Definizioni in Concept Learning

#### Concetto e consistenza

- Un concetto in uno spazio delle istanze $\mathcal{X}$ è definito come una funzione booleana su $\mathcal{X}$, ovvero della forma $c:\mathcal{X}\rightarrow\{0,1\}$;
- Un esempio di un concetto $c$ su uno spazio delle istanze $\mathcal{X}$ è definito come una coppia $(x,c(x))$, dove $x\in\mathcal{X}$ e $c$ è una funzione booleana su $\mathcal{X}$;
- Sia $h:\mathcal{X}\rightarrow\{0,1\}$ una funzione booleana su $\mathcal{X}$, diciamo che $h$ soddisfa $x\in\mathcal{X}$ se $h(x)=1$;
- Sia $h:\mathcal{X}\rightarrow\{0,1\}$ una funzione booleana su $\mathcal{X}$ e $(x,c(x))$ un esempio di $c$, allora diciamo che $h$ è consistente con l'esempio se $h(x)=c(x)$;
- Similmente un'ipotesi $h$ è consistente con un insieme di esempi $D$ se e solo se $\forall\langle x,c(x)\rangle.h(x)=c(x)$ e si indica con $h\triangleright D$.

#### Struttura dello spazio delle ipotesi

Siano $h_i$ e $h_j$ funzioni booleane definite su uno spazio delle istanze $\mathcal{X}$. Diciamo che $h_i$ è più generale o equivalente di $h_j$ con la notazione $h_i\geq_g h_j$ se e solo se:
$$
\forall x\in\mathcal{X}.[(h_j(x)=1)\implies(j_i(x)=1)]
$$

### Algoritmo FIND-S

Consideriamo lo spazio delle ipotesi delle configurazioni di $m$ letterali positivi e negativi.
L'algoritmo FIND-S trova l'ipotesi più specifica che è consistente con gli esempi di apprendimento:

- Input: Insieme di apprendimento $T_r$
- Inizializza $h$ con l'ipotesi più specifica
- Per ogni istanza di apprendimento positiva $(x,True)\in T_r$, rimuovi da $h$ ogni letterale non soddisfatto in $x$
- Restituisci $h$

#### Osservazioni

- FIND-S può essere adattato ad altri spazi delle istanze e delle ipotesi.
- Quando l'ipotesi corrente $h_i$ viene generalizzata con un'ipotesi $h_{i+1}$ con $h_{i+1}\geq_g h_i$, tutti gli esempi positivi presentati in precedenza continuano ad essere soddisfatti: poiché $h_{i+1}\geq_g h_i$ allora $\forall x\in\mathcal{X}.(h_i(x)=1)\implies(h_{i+1}(x)=1)$.
- Se il concetto target $c$ è contenuto nello spazio delle ipotesi, allora tutti gli esempi negativi rimarranno consistenti con l'ipotesi corrente durante tutta l'evoluzione dell'algoritmo. Questo perché ad ogni passo l'ipotesi corrente è più specifica consistente con gli esempi positivi visti fino a quel momento, ovvero assegna il minor numero di $1$ alle istanze in $\mathcal{X}$

#### Limiti di FIND-S

##### Convergenza al concetto target

Non c'è modo di determinare se la regola trovata da FIND-S è l'unica in $H$ consistente con gli esempi, oppure se ci sono altre ipotesi consistenti con gli esempi di training.

#### Ipotesi più specifica

L'ipotesi più specifica non è necessariamente la migliore. Perché non scegliere invece la più generale o una di generalità intermedia?

#### Consistenza degli esempi

Nelle applicazioni pratiche gli esempi di training possono contenere rumore. FIND-S non è resistente al rumore e può convergere ad un'ipotesi inconsistente con gli esempi negativi.

#### Unicità della soluzione

Può essere che esistano più ipotesi massimamente specifiche, in questo caso FIND-S dovrebbe essere esteso per prevedere la possibilità di esplorare un ramo di scelte diverso da quello preso originariamente.

### Candidate-Elimination

Vogliamo trovare tutte le ipotesi consistenti con un insieme di apprendimento, ovvero il Version Space. Definiamo dunque l'algoritmo Candidate-Elimination nel modo seguente:

- Partire da un version space uguale all'intero spazio delle ipotesi.
- Usare gli esempi positivi per rimuovere le ipotesi troppo specifiche dal versione space candidato
- Usare gli esempi negativi per rimuovere le ipotesi troppo generali dal version space candidato

#### Definizioni in Candidate-Elimination

- Version Space: sottoinsieme delle ipotesi in $H$ consistente con gli esempi di training.
- General boundary: l'insieme dei membri di $H$ di massima generalità consistenti con $D$, ovvero $G=\{g\in H\mid g\triangleright D \land(\not\exists g'\in H.((g'>_g g)\land (g'\triangleright F)\}$.
- Specific boundary: l'insieme dei membri di $H$ di massima specificità consistenti con $D$, ovvero $S=\{s\in H\mid s \triangleright D \land(\not\exists s'\in H.((s>_g s')\land (s'\triangleright F)\}$.
- Dati $G$ e $S$, si può dimostrare che un'ipotesi $h$ appartiene al Version Space se e solo se $\exists s\in S. \exists g\in G. g\geq_g h\geq_g s$.

## Alberi di decisione

In molte applicazioni reali l'apprendimento di concetti non è sufficiente, poiché in grado di rappresentare solo congiunzioni di letterali esenti da rumore.

Gli alberi di decisione sono un'ottima alternativa: permettono di apprendere funzioni/regole di decisione rappresentabili con alberi.
Gli alberi di decisione possono essere facilmente tradotti in una serie di regole di tipo if-then-else, rendendo questa rappresentazione delle ipotesi facilmente comprensibile per un umano.
Quest'ultima caratteristica li rende particolarmente interessanti per applicazioni di tipo medico, biologico, finanziario ecc. dove risulti necessario per l'utente esperto poter interpretare il risultato di un algoritmo di apprendimento.

### Definizioni in Alberi di decisione

In un albero di decisione:

- Ogni nodo interno effettua un test su un particolare attributo;
- Ogni ramo uscente da un nodo corrisponde ad uno dei possibili valori che l'attributo può assumere;
- Ogni foglia assegna una classificazione.

La classificazione di un'istanza avviene nel modo seguente:

- Partiamo dalla radice;
- Selezioniamo l'attributo associato al nodo corrente;
- Seguiamo il rampo associato al valore di quel attributo nell'istanza;
- Se abbiamo raggiunto una foglia restituiamo l'etichetta associata alla foglia, altrimenti ripetiamo dal secondo punto, partendo dal nodo corrente.

### Quando usare gli Alberi di decisione

Gli alberi di decisione sono particolarmente adatti a trattare:

- Istanze rappresentate da coppie attributo-valore
  - Insieme fissato di attributi e valori
  - Pochi valori possibili per gli attributi
  - Attributi a valori discreti, ma anche reali con opportune modifiche
- Funzioni target con valori di output discreti (anche più di 2)
- Concetti target descritti da disgiunzioni di funzioni booleane
- Esempi di apprendimento che possono contenere errori e/o valori mancanti

Gli algoritmi di apprendimento per alberi di decisione sono in genere efficienti.

### Alberi di decisione e funzioni booleane

Un albero di decisione può essere rappresentato da una funzione booleana:

- Ogni cammino dalla radice ad una foglia codifica una congiunzione di test su attributi;
- Più cammini che conducono allo stesso tipo di classificazione codificano una disgiunzione di congiunzioni;
- Le due regole sopra definiscono una serie di DNF, una per classe.

### Algoritmo ID3

l'Apprendimento di alberi di decisione tipicamente procede attraverso una procedura di tipo divide-et-impera che costruisce l'albero top-down in modo ricorsivo.
Presi $S$ l'insieme degli esempi e $A$ l'insieme degli attributi:

- Crea il nodo radice $T$;
- Se gli esempi in $S$ sono tutti della stessa classe $c$, ritorna $T$ etichettato con la classe $c$;
- Se $A$ è vuoto, ritorna $T$ con etichetta la classe di maggioranza in $S$;
- Scegli $a\in A$, l'attributo "ottimo" in $A$;
- Partiziona $S$ secondo i possibili valori che $a$ può assumere: $S_{a=v_1},\ldots,S_{a=v_m}$ dove $m$ è il numero di valori distinti possibili dell'attributo $a$;
- Ritorna l'albero $T$ avente come sotto-alberi gli alberi ottenuti richiamando ricorsivamente $ID3(S_{a=v_j,A\setminus a})$, per ogni $j$.

### Scelta dell'attributo ottimo in ID3

Gli algoritmi di apprendimento di alberi di decisione si differenziano soprattutto dal modo in cui si seleziona l'attributo ottimo: ID3 utilizza i concetti di Entropia e Guadagno di informazione.

#### Entropia

L'Entropia è una misura del grado di impurità di un inseme di esempi $S$.
Sia $C$ il numero di classi e $S_c$ il sottoinsieme di $S$ di esempi di classe $c$, l'entropia è calcolata con la formula
$$
E(S)=-\sum_{x=1}^{m}p_c\log{p_c}\text{, dove }p_c=\frac{|S_c|}{|S|}
$$
che nel caso della classificazione binaria diventa:
$$
E(S)=-p_-\log(p_-)-p_+\log(p_+)
$$
con $p_-$ e $p_+$ la proporzione di esempi negativi e positivi in $S$

#### Information Gain

L'Information Gain è la riduzione attesa di entropia che si ottiene partizionando gli esempi in $S$ usando l'attributo $a$:
$$
G(S,a)=E(s)-\sum_{v\in V(a)}\frac{|S_{a=v}|}{|S|}E(S_{a=v})
$$

#### Criteri alternativi per la scelta dell'attributo ottimo

È possibile definire la nozione di Information Gain sulla base di altre misure di impurità

- Cross-Entropy: $i_H=-\sum_{c=1}^mp_c\log{p_c}$
- Gini Index: $I_G=1-\sum_{c=1}^m p_c^2$
- Misclassification: $I_E=1-max_c(p_c)$

Sia $I$ uno qualsiasi di questi criteri di impurità, analogamente a quanto detto in precedenza, possiamo definire il guadagno di informazione come:
$$
G(S,a)=I(S)\sum_{v\in V(a)}\frac{|S_{a=v}|}{|S|}I(S_{a=v})
$$

### Controindicazioni sull'uso dell'Information Gain

L'Information Gain tende a favorire troppo gli attributi che possono assumere tanti valori diversi. Un esempio di questo comportamento lo si ha quanto viene introdotto un attributo temporale, una data, al training set. L'Information Gain ottenuto dividendo le istanze in base a questo attributo è massimo, dato che si stanno costruendo sottoinsiemi diversi e puri, quindi con impurità nulla. anche se in realtà quel attributo non è significativo.

### Bias induttivo negli Alberi di Decisione

Come per altri algoritmi induttivi, anche ID3 può essere visto come ricerca di un'ipotesi che "fitta" i dati in uno spazio delle ipotesi.

- Lo spazio delle ipotesi è l'insieme dei possibili alberi di decisione.
  Nota che le regole corrispondenti sono un sotto-insieme delle possibili DNF definite sugli attributi delle istanze
- La ricerca è di tipo hill climbing. Si inizia dall'albero vuoto e si procede con alberi via via più elaborati, fermandosi non appena ne troviamo uno consistente con gli esempi.
  Per cui, applicando il principio del guadagno informativo, ne segue che:
  - Gli alberi con un numero minore di nodi sono preferiti rispetto ad alberi più grandi;
  - Gli attributi con alto guadagno informativo sono più vicini alla radice.

### Attributi continui

Per utilizzare Alberi di Decisione su attributi continui è possibile creare dinamicamente uno pseudo-attributo booleano $A_c$ che è vero se $A<c$ e falso altrimenti.
Una possibilità è scegliere il valore $c$ che corrisponde al guadagno informativo massimo.
È dimostrabile che il valore ottimale, che massimizza il guadagno, si localizza nel valore di mezzo tra due valori con target diverso.
Sarà poi possibile riutilizzare lo stesso attributo per futuri tagli nello stesso cammino.

### Attributi con valori mancanti

Nelle applicazioni pratiche può succedere che, per alcuni esempi, alcuni attributi non abbiano un valore assegnato.
Dato un insieme di esempi $\hat{Tr}$ e un esempio $(x,y)$ a cui manca il valore di un attributo $A$, possiamo procedere nei seguenti modi:

1. Utilizzare per $A$ il valore più frequente in $\hat{Tr}$;
2. Come nel caso precedente, ma considerando solo esempi con target $y$;
3. Considerando tutti i valori $v_i\in V(A)$ e la loro probabilità di occorrere $P(v_i\mid\hat{Tr})$, stimata su $\hat{Tr}$. Quindi, sostituire l'esempio $(x,y)$ con $|V(A)|$ "istanze frazionarie", una per ogni valore $v_i$ e peso uguale a $p(v_i\mid\hat{Tr})$.

### Overfitting negli alberi di decisione

#### Reduced Error Pruning

- Dividere $Tr$ in $Tr'$ e $Va$, di cui il secondo è detto validation set, e $Tr = Tr'\cup Va$;
- Ripetere fino a quando le prestazioni peggiorano:
  - Per ogni nodo interno valutare l'accuratezza su $Va$ nel caso il sotto-albero radicato nel nodo venga potato;
  - Effettuare la potatura che corrisponde alla miglior performance sul validation set.

In questo caso effettuare la potatura significa sostituire, al posto di un sotto-albero radicato in un nodo, un nodo foglia etichettato con l'etichetta più frequente negli esempi associati a quel nodo.

#### Rule-Post Pruning

L'idea di base è trasformare l'albero di decisione in un insieme di regole if-then e poi effettuare la potatura delle regole:

- Si genera una regola $R_i$ per ogni cammino $path(r,f_i)$ dalla radice $r$ alla foglia i-esima $f_i$. R_i sarà della forma:
   $$
   \text{IF }(Att_{i_1}=v_{i_1})\cap(Att_{i_2})\cap\ldots\cap(Att_{i_k}=v_{i_k})\text{ THEN }label_{f_i}
   $$

- Si effettua la potatura indipendentemente su ogni $R_i$:
  - Si stimano le prestazioni ottenute usando solo $R_i$ come classificatore;
  - Si rimuovono le precondizioni che conducono ad un aumento della stima delle prestazioni sul validation set utilizzando un approccio greedy.
- Si ordinano le $R_i$ potate in ordine decrescente di prestazione; eventualmente si aggiunge una regola di default che restituisce la classe più frequente.

La classificazione avviene seguendo l'ordine stabilito dalle regole:

- La prima regola la cui precondizione è soddisfatta dalla istanza viene usata per generare la classificazione;
- Se nessuna regola ha le precondizioni soddisfatte, si utilizza la regola di default per classificare l'istanza, restituendo la classe più frequente nell'insieme di training.

#### Considerazioni su Rule-Post Pruning

- La stima delle prestazioni necessaria per effettuare la potatura può essere fatta sia usando un insieme di validazione che utilizzando un test statistico sui dati di apprendimento.
- La trasformazione Albero $\rightarrow$ Regole permette di generare regole dove si possono considerare contesti per un nodo che non necessariamente contengono i nodi antecedenti (e in particolare la radice), stiamo cambiando lo spazio delle ipotesi in effetti.
- Rispetto agli alberi, di solito es regole sono più semplici da comprendere per un umano.
- Di solito, Rule-Post Pruning riesce a migliorare le performance rispetto all'albero su cui è applicato e si comporta meglio del Reduced-Error Pruning.

## Reti neurali

### Quando usare RNA

- Input: discreto e/o a valori reali, alta dimensionalità;
- Output: vettore di valori discreti o reali;
- I dati in input e/o in output possono contenere rumore e la forma della funzione target totalmente sconosciuta;
- Accettabile avere tempi lunghi di apprendimento e richiesta una veloce valutazione della funzione appresa;
- La soluzione finale non deve necessariamente essere comprensibile da un esperto umano.

### Il Perceptron

Consideriamo lo spazio degli iperpiani in $\mathbb{R}^n$ con $n$ la dimensione dell'input, allora:
$$
\mathcal{H}=\{f_{(w,b)}=sign(\mathbf{w}\cdot\mathbf{x}+b)\mid \mathbf{w},\mathbf{x}\in\mathbb{R}^n,b\in\mathbb{R}\}
$$
che possiamo riscrivere come:
$$
\mathcal{H}=\{f_{w'}(x')=sign(\mathbf{w}'\cdot\mathbf{x}')\mid\mathbf{w}',\mathbf{x}'\in\mathbb{R}^n\}
$$
effettuando le seguenti trasformazioni:
$$
\mathbf{w}'=[b,\mathbf{w}], x'=[1,\mathbf{w}]
$$

### Funzioni booleane e Perceptron

Consideriamo input binari e funzioni booleane.

- Ogni funzione booleana può essere rappresentata tramite gli operatori or, and e not.
- Il perceptron può rappresentare l'operatore **or** se i pesi sono $\mathbf{w}=[-0.5,1,1,1\ldots]$
- Il perceptron può rappresentare l'operatore **and** se i pesi sono $\mathbf{w}=[-n+0.5,1,1,1\ldots]$
- Il perceptron può rappresentare l'operatore **not** se i pesi sono $\mathbf{w}=[1,-1]$
- Il perceptron non può rappresentare l'operatore **xor**, questo può essere giustificato con quanto detto riguardo la VC-dimension dello spazio di iperpiani in $\mathbb{R}^n$: xor non è rappresentabile da una retta su $\mathbb{R}^2$, serve uno spazio più grande.

### Algoritmo di apprendimento del Perceptron

Assumiamo di avere esempi di apprendimento linearmente separabili, ovvero tali che esiste un iperpiano che li possa separare, allora:

- Input: insieme di apprendimento $S=\{(\mathbf{x},t)\},\mathbf{x}\in\mathbb{R}^{n+1},t\in\{-1,+1\},\eta\geq 0$ dove $\eta$ è il learning rate.
- Inizializza il valore dei pesi $\mathbf{w}$ ad un vettore random
- Ripeti finché non si stabilizza:
  - Seleziona randomicamente uno degli esempi di apprendimento $(\mathbf{x},t)$
  - Se $o=sign(\mathbf{w}\cdot\mathbf{x})\neq t$ allora $\mathbf{w}\leftarrow\mathbf{w}+\eta(t-o)\mathbf{x}$

### Osservazioni sul l'algoritmo del perceptron

- L'algoritmo non termina necessariamente in un solo passo;
- Il coefficiente $\eta$ serve per rendere più stabile l'apprendimento, ovvero evita che il vettore dei pesi subisca variazioni troppo violente durante l'assegnamento;
- Se l'insieme di apprendimento è linearmente separabile, si dimostra che l'algoritmo di apprendimento per il Perceptron termina con una soluzione in un numero finito di passi;
- Sia $R$ il raggio della più piccola iper-sfera centrata nell'origine che contiene tutti gli esempi, i.e. $\parallel x_i\parallel\leq R$, sia $\gamma$ il massimo valore tale che $y_i(\mathbf{w}\cdot\mathbf{x}_i)\geq\gamma>0$, allora il numero di passi dell'algoritmo del Perceptron è limitato superiormente dal rapporto $R^2/\gamma^2$

### Componenti del Perceptron

Somma dei pesi: $net=\sum_{i=0}^n\mathbf{w}_i\mathbf{x}_i$

Funzione di attivazione:

- Lineare: $o=\sigma(net)=net$ ;
- Hard-threshold: $o=\sigma(net)=sign(net)$ ;
- Sigmoide: $o=\sigma(net)=\frac{1}{1+e^{-net}}$ .

### Reti Neurali Multistrato

Una RNA multistrato è un sistema costituito da unità interconnesse che calcolano funzioni non lineari:

- Le unità di input rappresentano le variabili in ingresso;
- Le unità di output rappresentano le variabili in uscita;
- Le unità nascoste rappresentano variabili interne che codificano le correlazioni tra le variabili di input, relativamente al valore di output che si vuole generare;
- Sulle connessioni tra unità sono definiti pesi adattabili che vengono alterati dall'algoritmo di apprendimento.

Le reti multistrato permettono di rappresentare superfici di decisione molto più complesse rispetto a quelle di singoli perceptron.

### Regola Delta per Perceptron

L'algoritmo Perceptron trova un vettore dei pesi rappresentate un iperpiano capace di separare i dati, se questi sono linearmente separabili.

La regola Delta è una regola di aggiornamento dei pesi diversa da quella del Perceptron che ci permette di ottenere una soluzione che approssima "al meglio" il concetto target.

In particolare, essa usa la discesa di gradiente per esplorare lo spazio delle ipotesi e selezionare l'ipotesi che approssima meglio il concetto target, minimizzando una funzione di errore di apprendimento opportunamente definita.

Considerando un Perceptron con attivazione lineare e definiamo una misura dell'errore commesso da un particolare vettore dei pesi $\mathbf{w}$, lo scarto quadratico medio, come:
$$
E[\mathbf{w}]=\frac{1}{2N}\sum_{(x^{(s)},t^{(s)})\in S}(t^{(s)}-o(\mathbf{x}^{(s)}))^2
$$

dove $N$ è la cardinalità dell'insieme di apprendimento $S$.
$$
\forall(\mathbf{x}^{(s)},t^{(s)})\in S,o(\mathbf{x}^{(s)})=t^{(s)}\implies E[\mathbf{W}]=0
$$
quindi bisogna minimizzare $E[\mathbf{W}]$ rispetto a $\mathbf{w}$.

L'algoritmo consiste nel partire da un $\mathbf{w}$ random e modificarlo nella direzione contraria al gradiente, che indica la direzione di crescita di $E[\mathbf{w}]$ :
$$
\nabla E[\mathbf{w}]\equiv[\frac{\partial E}{\partial w_0},\frac{\partial E}{\partial w_1},\ldots,\frac{\partial E}{\partial w_n}]
$$
$$
\Delta\mathbf{w}=-\eta\nabla E[\mathbf{w}]
$$
$$
\Delta w_i=-\eta\frac{\partial E}{\partial w_i}
$$
$$
\frac{\partial E}{\partial w_i}=-\frac{1}{N}\sum_{s=1}^N(t^{(s)}-o^{(s)})x_i^{(s)}
$$

### Algoritmo con discesa di gradiente

Ogni esempio di apprendimento è una coppia $(\mathbf{x},t)$ dove $\mathbf{x}$ è il vettore di valori in input e $t$ è il valore desiderato in output (target). $\eta$ è il coefficiente di apprendimento che ingloba il termine costante $1/N$

- Assegna a $w_i$ valori piccoli random
- Finché la condizione di terminazione non è verificata:
  - $\Delta w_i\leftarrow 0$
  - Per ogni $\mathbf{x},t\in S$:
    - Presenta $\mathbf{x}$ al neurone e calcola l'output $o(\mathbf{x})=\mathbf{w}\cdot\mathbf{x}$
    - Per ogni $i\in\{1,\ldots,n\}$ applico $\Delta w_i\leftarrow\Delta w_i+\eta(t-o)\mathbf{x}_i$
    - Per ogni $i\in\{1,\ldots,n\}$ applico $w_i\leftarrow w_i+\Delta w_i$

### Attivazione con sigmoide

Consideriamo un Perceptron con funzione di attivazione sigmoidale.
Seguiamo lo stesso approccio di prima, ovvero vogliamo trovare il vettore dei pesi che minimizza lo scarto quadratico medio sul training set utilizzando un algoritmo basato sulla discesa di gradiente.

A questo scopo si nota che per la funzione $\sigma(y)=\frac{1}{1+e^{-y}}$ definita sopra vale la seguente relazione:
$$
\frac{\partial\sigma(y)}{\partial y}=\sigma(y)(1-\sigma(y))
$$

allora vale:
$$
\frac{\partial E}{\partial w_i}=-\frac{1}{N}\sum_{s=1}^N(t^{(s)}-o^{(s)})\sigma(y^{(s)})(1-\sigma(y^{(s)}))x_i^{(s)}
$$

Dunque l'aggiornamento dei pesi dell'algoritmo dato sopra diventa $\Delta w_i\leftarrow\Delta w_i+\eta(t-o)\sigma(y)(1-\sigma(y))x_i$

### Terminologia

- $d$ unità di ingresso: dimensione dei dati in ingresso $\mathbf{x}\equiv(x_1,\ldots,x_d)$, $d+1$ se includiamo la soglia del vettore dei pesi $\mathbf{x}'\equiv(x:0,x\frac{\partial E}{\partial w_1}1,\ldots,x_d)$;
- $N_H$ unità nascoste, con output $\mathbf{y}\equiv(y_1,\ldots,y_{N_H})$;
- $c$ unità di output: dimensione dei dati di output $\mathbf{z}\equiv(z_1,\ldots,z_c)$ e dimensione degli output desiderati $\mathbf{t}=(t_1,\ldots,T_c)$;
- $w_{ji}$: peso dell'unità di ingresso $i$ all'unità nascosta $j$. $\mathbf{w}_j$ è il vettore dei pesi dell'unità nascosta $j$;
- $w_{kj}$: peso dell'unità di nascosta $i$ all'unità di output $k$. $\mathbf{w}_k$ è il vettore dei pesi dell'unità di output $k$;

### Calcolo gradiente per i pesi di una unità di output

$$
\frac{\partial E}{\partial w_{\hat{k}\hat{j}}}=-\frac{1}{cN}\sum_{s=1}^N(t_{\hat{k}}^{(s)}-z_{\hat{k}}^{(s)})z_{\hat{k}}(1-z_{\hat{k}})y_{\hat{j}}^{(s)}
$$

### Calcolo gradiente per i pesi di una unità nascosta

$$
\frac{\partial E}{\partial w_{\hat{j}\hat{i}}}=-\frac{1}{cN}\sum_{s=1}^{N}y_{\hat{j}}^{(s)}(1-y_{\hat{j}}^{(s)})x_{\hat{i}}^{(s)}\sum_{k=1}^c
(t_k^{(s)}-z_k^{(s)})z_k^{(s)}(1-z_k^{(s)})w_{k\hat{j}}
$$

### Discesa Batch e Stocastica

- **Batch**: finché la condizione di terminazione non è soddisfatta:
  - Calcola $\nabla E_S[\mathbf{w}]$
  - $\mathbf{w}\leftarrow\mathbf{w}-\eta\nabla E_S[\mathbf{w}]$
- **Stocastica**: finché la condizione di terminazione non è soddisfatta, per ogni esempio di training $s$:
  - Calcola $\eta E_s[\mathbf{w}]$
  - $\mathbf{w}\leftarrow\mathbf{w}-\eta\nabla E_s[\mathbf{w}]$
- **Mini-batch**: finché la condizione di terminazione non è soddisfatta, consideriamo un sottoinsieme di esempi $Q\subseteq S$:
  - Calcola $\eta E_Q[\mathbf{w}]$
  - $\mathbf{w}\leftarrow\mathbf{w}-\eta\nabla E_Q[\mathbf{w}]$

  dove in generale $E_Q[\mathbf{w}]=\frac{1}{2cN}\sum_{s\in Q}\sum_{k=1}^c(t_k^{(s)}-z_k^{(s)})^2,Q\subseteq S$

### Algoritmo di Backpropagation con uno strato nascosto

- Inizializza tutti i pesi a valori random piccoli;
- Finché non è verificata la condizione di terminazione:
  - Per ogni $(\mathbf{x},\mathbf{t})\in S$
    - Presenta $\mathbf{x}$ alla rete e calcola i vettori $\mathbf{y}$ e $\mathbf{z}$
    - Per ogni unità di output $k$ calcolo:
      - $\delta_k=z_k(1-z_k)(t_k-z_k)$
      - $\Delta w_{kj}=\delta_ky_j$
    - Per ogni unità nascosta $j$ calcolo:
      - $\delta_j=y_j(1-y_j)\sum_{k=1}^c w_{kj}\delta_k$
      - $\Delta_{wj}=\delta_jx_i$
    - Aggiorna tutti i pesi: $w_{sq}\leftarrow w_{sq}+\eta\Delta w_{sq}$

### Universalità delle reti multi-strato

#### Teorema di Pinkus, 1996 (semplificato)

Data una rete feed-forward con un solo livello nascosto, una qualsiasi funzione continua $f:\mathbb{R}^n\rightarrow\mathbb{R}$, un qualunque $\epsilon>0$ piccolo a piacere, per un'ampia classe di funzioni di attivazione, esiste sempre un intero $M$ tale che la funzione $g:\mathbb{R}^n\rightarrow\mathbb{R}$ calcolata dalla rete usando almeno $M$ unità nascoste approssima la funzione $f$ con tolleranza $\epsilon$, ovvero:
$$
max_{\mathbf{x}\in\Omega}|f(\mathbf{x})-g(\mathbf{x})|<\epsilon
$$
Da notare che il teorema afferma l'esistenza di tale rete, ma non dice nulla su come tale numero $M$ possa essere calcolato.

### Problemi nelle reti multi-strato

- La scelta della tipologia della rete determina lo spazio delle ipotesi che si utilizza. Con architetture a 3 livelli (input, hidden, output) il numero delle unità nascoste determina la complessità dello spazio delle ipotesi;
- La scelta del passo di discesa $\eta$ può essere determinante per la convergenza o rallentare enormemente l'apprendimento se non correttamente pesato;
- L'apprendimento è generalmente lento;
- Minimi locali, anche se nella pratica il problema è limitato.

### Evitare i minimi locali

Per evitare casi di minimo locale si posso applicare le seguenti strategie:

- Aggiungere un termine momento alla regola degli update dei pesi, tale componente applica un contributo proporzionale al passo precedente imponendo una specie di inerzia al sistema;
- Usare l'apprendimento stocastico invece che batch può facilitare l'uscita da minimi locali;
- Apprendimento di reti diverse sugli stessi dati con inizializzazioni differenti e successiva selezione della rete più efficace;
  Alternativamente possiamo considerare un "comitato" di reti in cui la predizione è una media pesata delle predizioni delle singole reti.

### Rappresentazione dei livelli nascosti

Un'importante caratteristica delle reti neurali multi-strato è che permettono di scoprire utili rappresentazioni dei dati di ingresso. In particolare l'output delle unità nascoste è un'efficace rappresentazione dell'input che permette una più semplice separazione dei dati in output.

### Overfitting

Aumentando il numero di aggiornamento dei pesi l'errore sul validation set prima diminuisce e poi incrementa.
All'inizio, con valori dei pesi piccoli in modulo si riescono a descrivere superfici di decisioni "lisce"; all'aumentare del valore assoluto la complessità delle superficie di decisione aumenta e con essa anche la possibilità di avere overfitting.

Una soluzione è monitorare l'errore su un insieme di dati di validazione. In alternativa è possibile utilizzare un termine aggiuntivo nella funzione errore dipendente dalla norma dei pesi (regolarizzazione).

## Modelli lineari

I modelli lineari sono tra i più importanti tipi di modelli in ML.
Un modello lineare è della forma:
$$
f_{\mathbf{w},b}(\mathbf{x})=\sum_{i=1}^m w_ix_i+b=\mathbf{w}\cdot\mathbf{x}+b
$$
o accorpando il primo valore:
$$
f_{\mathbf{w},b}(\mathbf{x})=\sum_{i=0}^m w_ix_i=\mathbf{w}\cdot\mathbf{x}
$$
dove $x_0=1$ è un feature artificiale ad-hoc.

Per la classificazione, viene restituito il segno, ovvero $h(\mathbf{x})=sign(f_\mathbf{w}(x))\in\{-1,+1\}$.

Per la regressione, viene restituito il valore esatto della funzione $h(\mathbf{x})=f_\mathbf{w}(\mathbf{x})\in\mathbb{R}$.

### Emulare l'algoritmo del Perceptron con un modello lineare

Possiamo definire il modello lineare $h_\mathbf{w}(\mathbf{x})=sign(\sum_{i=0}^nw_ix_i)=sign(\mathbf{w}\cdot\mathbf{x})$.

- Dato un training set $\{(\mathbf{x}_1,y_1),\ldots,(\mathbf{x}_n,y_n)\}$ scegliamo un punto mal classificato $\mathbf{x}_i$ per cui $sign(\mathbf{w}\cdot\mathbf{x}_i)\neq y_i$
- Aggiorno il vettore dei pesi $\mathbf{w}\leftarrow\mathbf{w}+y_i\mathbf{x}_i$
- Ripeto l'operazione finché tutti i punti non sono correttamente classificati.

### Regressione Lineare Multivariata

Dato un training set $\{(\mathbf{x}_1,y_1),\ldots,(\mathbf{x}_n,y_n)\}$ cerchiamo un'ipotesi $h_\mathbf{w}$, uno spazio lineare, che minimizza l'errore quadratico medio sul training set, ovvero:
$$
E(\mathbf{w})=\frac{1}{N}\sum_{i=1}^n(h_\mathbf{w}(\mathbf{x}_i)-y_i)^2=\frac{1}{n}\parallel\mathbf{X}\mathbf{w}-\mathbf{y}\parallel^2
$$

Minimizzando l'errore residuo troviamo:
$$
min_\mathbf{w}E(\mathbf{w})\equiv\frac{1}{n}\parallel\mathbf{X}\mathbf{w}-\mathbf{y}\parallel^2
$$
$$
\nabla E(\mathbf{w})=\frac{2}{n}\mathbf{X}^T(\mathbf{X}\mathbf{w}-\mathbf{y})=0
$$
$$
\mathbf{X}^T\mathbf{X}\mathbf{w}=\mathbf{X}^T\mathbf{y}
$$
allora $\mathbf{w}=\mathbf{X}'\mathbf{y}$ dove $\mathbf{X}'=(\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T$. $\mathbf{X}'$ è la pseudo-inversa di $\mathbf{X}$.

### Generalized Linear Models

In generale qualsiasi trasformazione non lineare $\mathbf{x}\overset{\phi}\rightarrow\mathbf{z}$ può essere applicata ai dati.
Un iperpiano, dunque anche un modello lineare, nello spazio trasformato corrisponderà alla superficie di decisione non lineare dello spazio originale.

### Margine di un iperpiano

Dato un iperpiano $\mathbf{w}+\mathbf{x}+b=0$, la distanza di un punto $\mathbf{x}$ dall'iperpiano può essere espressa dalla misura algebrica $g(\mathbf{x})=\mathbf{w}\cdot\mathbf{x}+b$

Possiamo scrivere $\mathbf{x}=\mathbf{x}_p+r\frac{\mathbf{x}}{\parallel\mathbf{w}\parallel}$, dove

- $\mathbf{x}_p$ è la proiezione normale di $\mathbf{x}$ sull'iperpiano;
- $r$ è la distanza algebrica desiderata, con valore positivo o negativo a seconda del lato del piano in cui il punto si trova.
- $g(\mathbf{x}_p)=0$

$$
g(\mathbf{x}_k)=\underbrace{\mathbf{w}\cdot\mathbf{x}_p+b}_{=0}+r\frac{\mathbf{w}\cdot\mathbf{w}}{\parallel\mathbf{w}\parallel}=r\parallel\mathbf{w}\parallel\implies r=\frac{g(\mathbf{x}_k)}{\parallel\mathbf{w}\parallel}=\frac{1}{\parallel\mathbf{w}\parallel}
$$
dunque il margine sarà $\rho=\frac{2}{\parallel\mathbf{w}\parallel}$.

### Support Vector Machines

L'idea di fondo è applicare il principio Structural Risk Minimization agli iperpiani.
Sappiamo che un iperpiano in $\mathbb{R}^m$ ha $VC=m+1$, ma aggiungendo altri vincoli possiamo migliorarlo.

#### Teorema Support Vector Machines

Sia $R$ il diametro della più piccola ipersfera contenente tutti i punti in input. L'insieme degli iperpiani ottimi descritti dall'equazione $\mathbf{w}\cdot\mathbf{x}+b=0$ ha VC-dimension $VC_{opt}$ superiormente limitata come nella disequazione:
$$
VC_{opt}\leq min\{\lceil\frac{R^2}{\rho^2}\rceil,m\}+1
$$

Considerando lo spazio delle ipotesi $\mathcal{H}_k=\{\mathbf{w}\cdot\mathbf{x}+b\mid\parallel\mathbf{w}\parallel^2\leq c_k\}$ dove $c_1<c_2<c_3<\ldots$ e dati linearmente separabili, allora l'errore empirico di $\mathcal{H}_k$ è $0$ per ogni $k$ e il bound sull'errore ideale può essere minimizzato massimizzando il margine di separazione.

### SVM nel caso separabile

Considerando di avere $n$ esempi linearmente separabili $\{(\mathbf{x}_i,y_i)\}_1^n$ è possibile trovare l'iperpiano ottimo risolvendo il seguente problema di ottimizzazione:
$$
min_{\mathbf{w},b}\frac{1}{2}\parallel\mathbf{w}\parallel^2\\\text{ con vincoli } \forall i\in\{1,\ldots,n\}:y_i(\mathbf{w}\cdot\mathbf{x}_i+b)\geq 1
$$
Questo è un problema di ottimizzazione quadratico convesso, dunque garantisce un'unica soluzione ed esistono svariati metodi per la sua soluzione.

#### Soluzione del problema di ottimizzazione nel caso separabile

Il problema descritto sopra è detto problema primale e può essere facilmente risolto usando la sua formulazione duale. Nel problema duale i moltiplicatori di Lagrange $\alpha\geq0$ sono associati a ciascun vincolo del problema primale, uno per ogni esempio:
$$
max_\alpha\sum_{i=1}^n\alpha_i-\frac{1}{2}\sum_{i,j=1}^n y_yy_j\alpha_i\alpha_j(\mathbf{x}_i\cdot\mathbf{x}_j)
$$

La maggior parte degli $\alpha_i$ saranno $0$ nella soluzione, quelli che non lo sono sono definiti vettori di supporto.

La soluzione primale è:
$$
\mathbf{w}=\sum_{i=1}^ny_i\alpha_i\mathbf{x}_i
$$
$$
b=y_k-\mathbf{w}\cdot\mathbf{x}_k\text{ per ogni }x_k\text{ tale che }\alpha_k>0
$$

dunque:
$$
h(\mathbf{x})=sign(\mathbf{w}\cdot\mathbf{x}+b)=sign(\sum_{i=1}^ny_i\alpha_i(\mathbf{x}_i\cdot\mathbf{x})+b)
$$

Da questo deriviamo che la funzione di decisione dipende solo dal prodotto tra i punti e altri punti del training set (i vettori di supporto).

### SVM nel caso non separabile

Il duale di questa formulazione è molto simile al precedente:
$$
max_\alpha\sum_{i=1}^n\alpha_i-\frac{1}{2}\sum_{i,j=1}^ny_iy_j\alpha_i\alpha_j(\mathbf{x}_i\cdot\mathbf{x}_j)\\\text{ con vincoli }\forall i\in\{1,\ldots,n\}:0\leq\alpha_i\leq C, \sum_{i=1}^ny_i\alpha_i=0
$$

La differenza principale è che le variabili duali sono superiormente limitate da $C$.
Il valore di $b$ si ottiene similmente al caso separabile.

Il parametro $C$ può essere utilizzato per controllare l'overfitting: con $C$ grande il modello è più portato a non rispettare i dati in input per ridurre il costo del margine, con $C$ piccolo è possibile avere un margine più alto allo stesso costo, restando più vicini ai dati in input.
Rimane il problema che la superficie di decisione descritta da un iperpiano è spesso insufficientemente complessa per approssimare efficacemente la funzione target.

### SVM con trasformazioni non lineari

Quando gli esempi non sono linearmente separabili è possibile applicare i seguenti due passi:

- Proiettare i vettori degli input dall'input space ad uno spazio più grande, il feature space, tramite una trasformazione non lineare;
- L'iperpiano ottimale viene calcolato nel feature space, utilizzando la formulazione con variabili di slack.

Il primo passo è giustificato dal teorema sulla separabilità di Cover, il quale dice che pattern non linearmente separabili trasformati in un feature space più grande hanno alta probabilità di essere linearmente separabili, se la trasformazione è non lineare e la dimensionalità del feature space è sufficientemente grande.

Il secondo passo è banalmente giustificato dal fatto che l'iperpiano ottimale minimizza la VC-dimension.

Possiamo assumere che una qualsiasi coordinata nel feature space sia generata da una funzione non lineare $\varphi_j(\cdot)$, dunque possiamo considerare $M$ funzioni $\varphi_j(\mathbf{\cdot})$ con $j=1,\ldots,M$. Dunque, un generico vettore $\mathbf{x}$ viene mappato nel vettore M-dimensionale: $\varphi(\mathbf{x})=[\varphi_1(\mathbf{x}),\ldots,\varphi_M(\mathbf{x})]$

Il secondo passo richiede di trovare l'iperpiano ottimo nel feature-space M-dimensionale.
Definiamo un iperpiano nel feature-space come $\sum_{j=1}^MW_j\varphi_j(\mathbf{x})+b=0$ o equivalentemente $\sum_{j=0}^MW_j\varphi_j(\mathbf{x})=\mathbf{w}\cdot\varphi(\mathbf{x})=0$ dove $\varphi_0(\mathbf{x})=1$ e $w_0=b$.

Poiché $\mathbf{w}=\sum_{k=1}^ny_k\alpha_h\overset{\rightarrow}{\varphi}(\mathbf{x}_k)$ definiamo l'equazione per gli iperpiani come $\sum_{K=1}^ny_k\alpha_k\varphi(\mathbf{x}_k)\cdot\varphi(\mathbf{x})=0$ dove $\varphi(\mathbf{x}_k)\cdot\varphi(\mathbf{x})$ rappresenta il prodotto nel feature space tra i vettori introdotti dalla k-esima istanza di training e dall'input $\mathbf{x}$.

### Funzioni Kernel

Una funzione Kernel $K(\cdot,\cdot)$ tale che $K(\mathbf{x}_k,\mathbf{x})=\varphi(\mathbf{x}_k)\cdot\varphi(\mathbf{x})=\sum_{j=0}^n\varphi_j(\mathbf{x}_k)\varphi_j(\mathbf{x})=K(\mathbf{x},\mathbf{x}_k)$, dunque $K$ è una funzione simmetrica.

Se troviamo tale funzione, possiamo calcolare la funzione di decisione nel feature space senza rappresentare esplicitamente i vettori nel feature space usando la forma:
$$
\sum_{k=1}^ny_k\alpha_kK(\mathbf{x}_k,\mathbf{x})
$$
Funzioni con queste proprietà esistono se alcune condizioni sono soddisfatte, dette condizioni di Mercer.

In generale una funzione Kernel che soddisfa le condizioni di Mercer rappresenta un prodotto tra due vettori generati da qualche trasformazione non lineare.
Da notare che non necessitiamo di conoscere tale trasformazione.

#### Esempi di funzioni Kernel

- Kernel lineare: $K(\mathbf{x},\mathbf{x}')=\mathbf{x}\cdot\mathbf{x}'$
- Kernel polinomiale di grado $p$: $K(\mathbf{x},\mathbf{x}')=(\mathbf{x}\cdot\mathbf{x}'+u)^2$
- Radial-Basis Function (RBF) Kernel: $K(\mathbf{x},\mathbf{x}')=e^{-\gamma\parallel\mathbf{x}-\mathbf{x}'\parallel^2}$

### SVM con funzioni Kernel

L'introduzione di funzioni Kernel non cambia la formulazione del problema:
$$
max_\alpha\sum_{i=1}^n\alpha_i-\frac{1}{2}\sum_{i,j=1}^ny_iy_j\alpha_i\alpha_jK(\mathbf{x}_i,\mathbf{x}_j)\\\text{ con vincoli }\forall i\in\{1,\ldots,n\}:0\leq\alpha_i\leq C, \sum_{i=1}^ny_i\alpha_i=0
$$
dove i valori kernel richiesti vengono calcolate tutte le coppie di vettori $K(\mathbf{x}_i,\mathbf{x}_j)$ con $i,j=1,\ldots,n$ e arrangiate in una matrice $\mathbf{K}\in\mathbb{R}^{n\times n}$ simmetrica e definita positiva nota come matrice kernel.

### Regressione con SVM

Per utilizzare SVM in problemi di regressione definiamo un $\epsilon$-tubo: un sottospazio per cui le predizioni che differiscono dal valore desiderato di più di un certo $\epsilon$ in valore assoluto vengono penalizzate linearmente, in caso contrario non sono considerate errori.

La formulazione dell'$\epsilon$-tubo è la seguente:
$$
min_{w,b,\xi,\xi^*}\frac{1}{2}\parallel\mathbf{w}\parallel^2+C\sum_{i=1}^n(\xi_i+\xi_i^*) \\
\text{con vincoli }\forall i\in\{1,\ldots,n\}: \\
y_i-\mathbf{w}\cdot\mathbf{x}_i-b\leq\epsilon+\xi_i \\
\mathbf{w}\cdot\mathbf{x}_i+b-y_i\leq\epsilon+\xi_i^* \\
\xi_i,\xi_i^*\geq0
$$

## Problemi pratici nell'apprendimento

### Bias e Varianza

- Il Bias misura la distorsione della funzione stimata: quanto il valore medio della funzione stimata si discosta dalla funzione target;
- La Varianza misura la dispersione della funzione stimata: quanto distanti sono i risultati della funzione stimata tra di loro.

### Underfitting e Overfitting

- Quando il modello è troppo semplice può non catturare la piena complessità dei dati. In questo casi si tende ad avere un alto Bias.
- Quando il modello è troppo complesso può dare troppa importanza a proprietà spurie dei dati. In questi casi si tende ad avere un'alta Varianza.

### Model selection

#### Hold-out

#### K-fold Cross Validation

### Valutazione di dati sbilanciati

L'uso della metrica accuracy può portare a opinioni sbagliate sul modello, in particolare se il dataset è sbilanciato l'accuracy cresce.

Per ovviare a questo problema si possono usare metriche alternative, come Precision, Recall e $F_1$

#### Tavola di Contingenza

|               | Relevant            | Not Relevant        |
| ------------- | ------------------- | ------------------- |
| Retrieved     | True Positive (TP)  | False Positive (FP) |
| Not Retrieved | False Negative (FN) | True Negative (TN)  |

$$
\text{Precision: }\pi=\frac{TP}{TP+FP}
$$
$$
\text{Recall: }\rho=\frac{TP}{TP+FN}
$$
$$
\text{Accuracy: }\alpha=\frac{TP+TN}{TP+TN+FP+FN}
$$

#### F-Measure

F-Measure è un trade-off tra Precision e Recall e può essere bilanciato a seconda di come è fatto il dataset.
$$
F_\beta=\frac{(1+\beta^2)\pi\rho}{\beta^2\pi+\rho}
$$
Nel caso di $\beta=1$ la metrica diventa:
$$
F_1=2\frac{\pi\rho}{\pi+\rho}
$$

### Da classificazione binaria a classificazione multiclasse

#### One vs Rest

Classificazione binaria che determina se un esempio fa parte di una certa classe o no.
Per una classificazione multiclasse in $n$ classi è sufficiente avere $n$ classificatori One vs Rest.

#### One vs One

Classificazione binaria che determina in quale, tra due possibili classi, è più probabile che l'esempio sia contenuto.
Per una classificazione multiclasse in $n$ classi sono necessari $(n\times n-1)/2$ classificatori One vs One, ma può funzionare meglio con alcuni modelli, come gli algoritmi Kernel che non scalano bene con il numero di esempi.

### Micro e Macro Averaging

Dati i risultati di una metrica, come precision e recall, per indicare un singolo valore finale si usa una media.

#### Macro Averaging

Utile quando il dataset è sbilanciato e si vuole dare la stessa importanza a tutte le classi.
$$
\frac{r_A+r_b+r_C}{3}=\ldots
$$

#### Micro Averaging

Utile quando si vuole avere un bias/più importanza verso la classe più popolata
$$
\frac{2+2+3}{4+3+3}=\ldots
$$

## Preprocessing

### Feature categoriche o simboliche

- Nominali, ovvero senza un ordine: nomi, colori, tipo, ecc.
- Ordinali, ovvero che non preservano distanze: gradi del'esercito, ecc.

### Feature quantitative o numeriche

- Intervalli, ovvero valori enumerabili: livello di apprezzamento da 0 a 10
- Ratio, ovvero valori reali: peso di una persona

### One-Hot Encoding

Rappresentazione di un insieme di variabili categoriche tramite un vettore binario con valore 1 sulla variabile del caso e 0 su tutte le altre.

### Codifica delle variabili continue

Il processo prevede:

- Standardizzazione (centering e/o variance scaling)
- Scaling in un range
- Normalizzazione

Sia $\hat{x}_j=\frac{1}{n}\sum_{i=1}^nx_{ij}$ e $\sigma_j=\sqrt{\frac{1}{n}\sum_{i=1}^n(x_{ij}-\hat{x}_j)^2}$

- Centering: $c(x_{ij})=x_{ij}-\hat{x}_j$;
- Standardizzazione: $s(x_{ij})=\frac{c(x_{ij})}{\sigma_j}$;
- Scaling: $h(x_{ij})=\frac{x_{ij}-x{\text{min},j}}{x_{\text{max},j}-x_{\text{min},j}}$;
- Normalizzazione: $g(x)=\frac{x}{\parallel x\parallel}$.

### Preprocessing per variabili continue

- Binarizer: mappa una variabile a $0$ o a $1$ seguendo una soglia;
- Imputer: inserisce valori mancanti basandosi su media, mediana, valore più frequente nella riga o nella colonna;
- FunctionTransformer: definizioni custom del preprocessing.

## Metodi Bayesiani

I metodi Bayesiani forniscono tecniche computazionali di apprendimento tali per cui:

- Gli esempi osservati vanno ad incrementare o decrementare la probabilità che un'ipotesi sia corretta;
- Permettono di combinare conoscenza pregressa sulle ipotesi con dati osservati;
- Producono predizioni di probabilità;
- Classificano multiple ipotesi, pesandole a seconda della loro probabilità;
- Definiscono il caso ideale di predizione ottimale, anche se non è trattabile computazionalmente;
- Sono utili per l'interpretazione di algoritmi probabilistici;
- Difficoltà pratica: necessitano della definizione di molte probabilità; se queste non sono disponibili inizialmente, devono essere stimate;
- Difficoltà pratica: sono computazionalmente costosi e richiedono molti esempi prima di una corretta stima dei parametri.

### Teorema di Bayes

$$
P(h\mid D)=\frac{P(D\mid h)P(h)}{P(D)}
$$
Dove:

- $P(h)$: probabilità a priori dell'ipotesi $h$;
- $P(D)$: probabilità a priori dei dati di apprendimento;
- $P(h\mid D)$: probabilità dell'ipotesi $h$ avendo i dati $D$;
- $P(D\mid h)$: probabilità di avere i dati $D$ data l'ipotesi $h$;

### Scelta dell'ipotesi

In generale si vuole selezionare l'ipotesi più probabile con i dati di apprendimento, detta ipotesi **maximum a posteriori** $h_{MAP}$:
$$
\begin{aligned}
h_{map}
   &=\text{arg }\text{max}_{h\in H}P(h\mid D) \\
   &=\text{arg }\text{max}_{h\in H}\frac{P(D\mid h)P(h)}{P(D)} \\
   &=\text{arg }\text{max}_{h\in H}P(D\mid h)P(h) \\
\end{aligned}
$$

Se infine assumiamo che la probabilità sia uniforme sulle ipotesi, vale che $\forall_{i,j}.P(h_i)=P(h_j)$, allora possiamo scegliere l'ipotesi **maximum likelihood** $h_{ML}$:
$$
h_{ML}=\text{arg }\text{max}_{h\in H}P(D\mid h)
$$

### Apprendimento brute force dell'ipotesi MAP

- Per ogni ipotesi $h\in H$ calcola la probabilità a posteriori $P(h\mid D)=\frac{P(D\mid h)P(h)}{P(D)}$
- Restituisci l'ipotesi $h_{MAP}$, con la probabilità a posteriori più alta $h_{MAP}=\text{arg }\text{max}_{h\in H}P(h\mid D)$

### Classificazione ottima di Bayes

Sia data una classe $v_j\in V$, otteniamo:
$$
P(v_j\mid D)=\sum_{h_i\in H}P(v_j\mid h_i)P(h_i\mid D)
$$
da cui deriva che la classificazione ottima di Bayes di una certa istanza è la classe $v_j\in V$  che massimizza tale probabilità, ovvero:
$$
v_\text{Bayes}=\text{arg }\text{max}_{v_j\in V}\sum_{h_i\in H}P(v_j\mid h_i)P(h_i\mid D)
$$
Il classificatore ottimo di Bayes può essere molto costoso da calcolare se ci sono molte ipotesi.

#### Algoritmo di Gibbs

- Scegliere un'ipotesi a caso, con probabilità $P(h\mid D)$;
- Usarla per classificare la nuova istanza.

Fatto piuttosto sorprendente: assumendo che i concetti target estratti casualmente da $H$ secondo una probabilità a priori $H$, allora:
$$
E[\epsilon_\text{Gibbs}]\leq2E[\epsilon_\text{Bayes}]
$$
Supponendo una distribuzione a priori uniforme sulle ipotesi corrette in $H$,

- Seleziona una qualunque ipotesi da $VS$, con probabilità uniforme;
- Il suo errore atteso non è peggiore del doppio dell'errore ottimo di Bayes.

### Classificatore Naive Bayes

Quando usarlo:

- Gli insiemi di dati di dimensione abbastanza grande;
- Gli attributi che descrivono le istanze sono condizionalmente indipendenti data la classificazione.

Ha avuto successo in particolare per diagnosi e classificazione di documenti testuali.

Definiamo la funzione target come $f:X\rightarrow V$ con istanze $x$ descritte da attributi $\langle a_1,a_2,\ldots,a_n\rangle$.

Il valore più probabile di $f(x)$ è:
$$
\begin{aligned}
V_{MAP}
   &= \text{arg }\text{max}_{v_j\in V}P(v_j\mid a_1,a_2,\ldots,a_n) \\
   &= \text{arg }\text{max}_{v_j\in V}\frac{P(a_1,a_2,\ldots,a_n\mid v_j)P(v_j)}{P(a_1,a_2,\ldots,a_n)} \\
   &= \text{arg }\text{max}_{v_j\in V}P(a_1,a_2,\ldots,a_n\mid v_j)P(v_j) \\
\end{aligned}
$$
Con l'assunzione (di Naive Bayes) che $P(a_1,a_2,\ldots,a_n\mid v_j)=\prod_i P(a_i\mid v_j)$ si arriva a:
$$
P(a_1,a_2,\ldots,a_n\mid v_j)=\prod_iP(a_i\mid v_j)
$$

#### Algoritmo Naive Bayes

Naive Bayes Learn(esempi)

- foreach valore target $v_j$
  - $\hat{P}(v_j)\leftarrow$ stima di $P(v_j)$ su esempi
  - foreach valore di attributo $a_j$ di ogni attributo $a$
    - $\hat{P}(a_i\mid v_j)\leftarrow$ stima di $P(a_i\mid v_j)$ su esempi
- return $\hat{P}(v_j),\hat{P}(a_i\mid v_j)\forall_{i,j}$

Classify New Instance(x)

- $v_{NB}=\text{arg }\text{max}_{v_j\in V}\hat{P}(v_j)\prod_i\hat{P}(a_i\mid v_j)$
- return $v_{NB}$

#### Considerazioni aggiuntive Naive Bayes

L'assunzione di indipendenza condizionale è spesso violata, ma il sistema funziona comunque.
Questo perché è sufficiente che l'ipotesi $h_{MAP}$ sia la stessa, non c'è bisogno che i valori di probabilità calcolati siano uguali, né che l'ordine di probabilità delle altre ipotesi sia lo stesso.

La probabilità a posteriori calcolata da Naive Bayes è spesso agli estremi: vicina a $1$ o a $0$ anche se non dovrebbe.

Se non c'è alcun valore target $v_j$ con attributo $a_k$ la sua probabilità va a $0$.
Per evitare questo problema possiamo usare la M-stima Bayesiana per $\hat{P}(a_i\mid v_j)$:
$$
\hat{P}(a_i\mid v_j)\leftarrow\frac{n_c+mp}{n+m}
$$
dove:

- $n$ è il numero di esempi di apprendimento per cui $v=v_j$;
- $n_c$ è il numero di esempi di apprendimento per cui $v=v_j$ e $a=a_i$;
- $p$ è la stima a priori per $\hat{P}(a_i\mid v_j)$, di solito $\frac{1}{|a_i|}$;
- $m$ è il peso dato a priori, cioè il numero di esempi virtuali.

### Algoritmo Expected Maximization

Quando utilizzarlo:

- dati solo parzialmente osservabili
- clustering non-supervisionato (con valore target non osservabile)
- apprendimento supervisionato (alcuni attributi con valori mancanti)

Alcuni esempi:

- apprendimento Reti Bayesiane
- apprendimento di modelli di Markov nascosti (Hidden Markov Models)

#### Funzionamento EM

Assumiamo che ogni istanza $x$ sia generata:

- scegliendo una delle Gaussiane con probabilità uniforme;
- generando un'istanza a caso secondo la Gaussiana scelta.

Date:

- istanza da $X$ generate da una mistura di $k$ Gaussiane;
- medie $\langle\mu_1,\ldots,\mu_k\rangle$ sconosciute delle $k$ Gaussiane (assumiamo $\sigma^2$ conosciuto e uguale per tutte le Gaussiane);
- non si sa quale istanza x i è stata generata da quale Gaussiana.

Determinare:

- stime maximum-likelihood di $\langle\mu_1,\ldots,\mu_k\rangle$

Ogni istanza può essere pensata nella forma $y_i = \langle x, z_{i1}, z_{i2}\rangle$ (caso k = 2),

dove:

- $z_{ij}$ è $1$ se l’esempio $i$ è stato generato dalla Gaussiana $j$, $0$ altrimenti
- $x_i$ osservabile
- $z_{ij}$ non osservabili

#### EM per stimare le k-medie

Scegliere a caso l'ipotesi $h=\langle\mu_1\mu_2\rangle$, poi ripetere:

- **Passo E**: Calcola il valore atteso $E[z_{ij}]$ di ogni variabile non osservabile $z_{ij}$, assumendo valga l'ipotesi corrente $h$
  $$
  \begin{aligned}
  E[x_{ij}]
      &= \frac{p(x=x_i\mid\mu=\mu_j)}{\sum_{n=1}^2p(x=x_i\mid\mu=\mu_n)} \\
      &= \frac{e^{-\frac{1}{\sigma^2}(x_i\mu_j)^2}}{\sum_{n=1}^2e^{-\frac{1}{\sigma^2}(x_i-\mu_n)^2}}
  \end{aligned}
  $$
- **Passo M**: Calcola la nuova ipotesi maximum-likelihood $h$, assumendo che il valore preso da ogni variabile non osservabile $z_{ij}$ sia il suo valore atteso (calcolato sopra)
  $$
  \begin{aligned}
  \pi_{ij}=\frac{E[z_{ij}]}{\sum_{i=1}^mE[z_{ij}]} \text{ and } \mu\leftarrow\sum_{i=1}^m\pi_{ij}x_i
  \end{aligned}
  $$

## Kernel e rappresentazione

### Rappresentazione con kernel

Sia dato un insieme di oggetti $S=\{x_1,x_2,\ldots,x_n\}$. Come possono essere rappresentati?

- Rappresentazione esplicita $\varphi(x)\rightarrow\mathcal{F}$
- Rappresentazione kernel (implicita):
  - $K:X\times X\rightarrow\mathbb{R}$, comparazione a coppie, funzione simmetrica
  - $S$ rappresentata dalla matrice simmetrica $\mathbf{K}=[K(x_i,x_j)]_{i,j}\in\mathbb{R^{n\times n}}$

#### Matrici Kernel e di Gram

Una funzione kernel è una funzione $K(\cdot,\cdot)$ tale che $\forall \mathbf{x},\mathbf{z}\in\mathcal{X}$, soddisfa $K(\mathbf{x},\mathbf{z})=\varphi(\mathbf{x})\cdot\varphi(\mathbf{z})$ dove $\varphi(\mathbf{x})$ è una mappatura da $\mathcal{X}$ a uno spazio $\mathcal{H}$, che è un prodotto interno o Hillbert.

Una matrice (o kernel) di Gram associata alla funzione kernel $K(\cdot,\cdot)$, valutata su un sottoinsieme finito di esempi $X=\{x_1,\ldots,x_n\}$ con $x_i\in\mathcal{X}$ è la matrice $\mathbf{K}\in\mathbb{R}^{n\times n}$ tale che $\mathbf{K}_{i,j}=K(\mathbf{x}_i,\mathbf{x}_j)$.
Tale matrice è simmetrica e definita positiva per definizione.

Ad esempio:
$$
\begin{aligned}
   \forall\mathbf{x},\mathbf{z}\in\mathbb{R}^2, K(\mathbf{x},\mathbf{z})
      &= (\mathbf{x}\cdot\mathbf{z})^2 \\
      &= (x_1z_1+x_2z_2)^2 \\
      &= \begin{bmatrix}
            x_1^2 \\
            x_2^2 \\
            \sqrt{2}x_1x_2
         \end{bmatrix}\cdot\begin{bmatrix}
            z_1^2 \\
            z_2^2 \\
            \sqrt{2}z_1z_2
         \end{bmatrix} \\
      &= \varphi(\mathbf{x})\cdot\varphi(\mathbf{z}) \\
      \text{dove }\varphi(\mathbf{x}),\varphi(\mathbf{z})\in\mathbb{R}^3
\end{aligned}
$$

### Vantaggi dell'uso di kernel

La rappresentazione con matrici kernel ha i seguenti vantaggi:

- Posso usare lo stesso algoritmo per diverse tipologie di dati;
- Modularità del design di kernel e algoritmi;
- Semplificazione dell'integrazione di diverse viste sui dati.

La dimensionalità dei dati dipende dal numero degli oggetti e non dalla loro dimensione vettoriale.

Le comparazioni tra oggetti risultato computazionalmente più semplici rispetto all'uso della rappresentazione esplicita dell'oggetto.

Molti dei metodi kernel, incluso SVM, possono essere interpretati come la soluzione del problema
$$
\text{arg min}_{f\in\mathcal{H}}\mathcal{L}(f(x_1),\ldots,f(x_n))+\Lambda\parallel f\parallel_\mathcal{H}
$$
dove:

- $\mathcal{L}$ è la funzione loss/costo associata all'errore empirico;
- La norma è la "liscezza"/smoothness della funzione, questa dipende dal kernel considerato e dal feature space;
- $\Lambda$ è un coefficiente di regolazione per il trade-off

Il problema avrà sempre una soluzione del tipo:
$$
f(x)=\mathbf{w}\cdot\varphi(x)=\sum_{i=1}^n\alpha_iK(x_i,x)
$$
Questo è un problema di ottimizzazione formulato in $n$ variabili.
Se $n\ll d$ allora abbiamo un vantaggio computazionale.

### Modularità dei kernel

In generale, quando usiamo i metodi kernel la struttura della soluzione è la seguente:
$$
\begin{bmatrix}
\text{n objects} \\
(x_1,y_1),\ldots,(x_n,y_n)
\end{bmatrix}
\rightarrow
\begin{bmatrix}
\text{Kernel} \\
K(x_i,x_j)
\end{bmatrix}
\rightarrow
\begin{bmatrix}
\text{Matrice} \\
\mathbf{K}(n\times n)
\end{bmatrix}
\rightarrow
\begin{bmatrix}
\text{Algoritmo} \\
w=A(\mathbf{K},Y)
\end{bmatrix}
\rightarrow
\begin{bmatrix}
\text{Output} \\
f_w(x)
\end{bmatrix}
$$

Ogni componente di questa pipeline può essere facilmente sostituito con uno analogo.

### Kernel Trick

Qualunque algoritmo per dati vettoriali che può essere espresso in termini di prodotto tra vettori può essere implicitamente eseguito nel feature space associato ad un kernel, semplicemente rimpiazzando i prodotti con valutazioni kernel:

- Kernelization: trasformare metodi lineari o basati su distanze in metodi kernel (es. Perceptron e kNN);
- Applicazione di algoritmi per dati vettoriali (SVM, Perceptron, ecc.) a dati non vettoriali, utilizzando kernel ad-hoc (es. kernel per strutture).

Dati due oggetti $x,z\in\mathcal{X}$, la distanza tra i due oggetti nel feature space è calcolata con $d(x,z)=\parallel\varphi(x)\varphi(z)\parallel$ dove:
$$
\begin{aligned}
d^2
   &= \parallel\varphi(x)-\varphi(z)\parallel^2 \\
   &= \varphi(x)\cdot\varphi(x)+\varphi(z)\cdot\varphi(z)-w\varphi(x)\cdot\varphi(z) \\
   &= K(x,x)+K(z,z)-2K(x,z)
\end{aligned}
$$
Allora, $d(x,z)=\sqrt{K(x,x)+K(z,z)-2K(x,z)}$. Inoltre in questa formulazione $\varphi(x)$ e $\varphi(z)$ non vengono usate esplicitamente.

### Tipici kernel per vettori

- Kernel lineare: $K(\mathbf{x},\mathbf{z})=\mathbf{x}\cdot\mathbf{z}$
- Kernel polinomiale: $K(\mathbf{x},\mathbf{z})=(\mathbf{x}\cdot\mathbf{z}+u)^p$
- Kernel esponenziale: $K(\mathbf{x},\mathbf{z})=e^{\mathbf{x}\cdot\mathbf{z}}$
- Radial Basis Function Kernel: $K(\mathbf{x},\mathbf{z})=e^{-\gamma\parallel\mathbf{x}-\mathbf{y}\parallel^2}$

### Proprietà di chiusura

Siano $K_1,K_2$ kernel definiti su $\mathcal{X}\times\mathcal{X}$ e $a\in\mathbb{R}^+,\phi:\mathcal{X}\rightarrow\mathbb{R}^N$ con $K_3$ un kernel su $\mathbb{R}^N\times\mathbb{R}^N$.
Allora

- $K(\mathbf{x},\mathbf{z})=K_1(\mathbf{x},\mathbf{z})+K_2(\mathbf{x},\mathbf{z})$ è un kernel;
- $K(\mathbf{x},\mathbf{z})=aK_1(\mathbf{x},\mathbf{z})$ è un kernel;
- $K(\mathbf{x},\mathbf{z})=K_1(\mathbf{x},\mathbf{z})\cdot K_2(\mathbf{x},\mathbf{z})$ è un kernel;
- $K(\mathbf{x},\mathbf{z})=K_3(\phi(\mathbf{x}),\phi(\mathbf{z}))$ è un kernel.

Un kernel può essere normalizzato semplicemente applicando:
$$
\tilde{K}(\mathbf{x},\mathbf{z})=\frac{K(\mathbf{x},\mathbf{z})}{\sqrt{K(\mathbf{x},\mathbf{x})K(\mathbf{z},\mathbf{z})}}
$$
In questo modo vale per il feature space che $\parallel\phi(\mathbf{x})\parallel=1$.

### Estensione dei kernel ad altri tipi di strutture

- Kernel per stringhe: calcola il numero di sottostringhe condivise tra due stringhe;
- Kernel per alberi: dati due alberi, calcola il numero di sottoalberi condivisi;
- Kernel per grafi: calcola i cammini comuni tra due grafi.

### Kernel Booleani

- Kernel per dati binari: calcolare il numero di proposizioni logiche condivise;
- Un kernel booleano è una funzione $k:\{0,1\}^n\times\{0,1\}^n\rightarrow\mathbb{N}$ tale che $k(\mathbf{x},\mathbf{z})=\langle\phi(\mathbf{x},\phi(\mathbf{z}))\rangle$ dove $\phi:\{0,1\}^n\rightarrow\{0,1\}^N$ mappa vettori binari in input in uno spazio formato da formule logiche.
- Il kernel lineare, applicato a dati binari, è il più semplice kernel, poiché calcola il numero di letterali booleani condivisi tra i due vettori in input.

#### Kernel congiuntivo

- Dati due vettori $\mathbf{x},\mathbf{z}\in\{0,1\}^n$ il kernel congiuntivo (C-kernel) di grado $c$ calcola il numero di congiunzioni di arità $c$ tra $\mathbf{x}$ e $\mathbf{z}$.
- Le feature nello spazio espanso sono tutte le possibili combinazioni senza ripetizione di $c$ oggetti presi da $n$;
- Il valore di queste feature sono calcolate come l'operatore logico **AND** tra le variabili coinvolte nella combinazione;
- Il kernel è formalmente calcolato come:
  $$
  k_\land^c(\mathbf{x},\mathbf{z})=\binom{\langle\mathbf{x},\mathbf{z}\rangle}{c}
  $$

#### Kernel disgiuntivo

- Dati due vettori $\mathbf{x},\mathbf{z}\in\{0,1\}^n$ il kernel congiuntivo (D-kernel) di grado $c$ calcola il numero di disgiunzioni di arità $d$ tra $\mathbf{x}$ e $\mathbf{z}$.
- Le feature nello spazio espanso sono tutte le possibili combinazioni senza ripetizione di $d$ oggetti presi da $n$;
- Il valore di queste feature sono calcolate come l'operatore logico **OR** tra le variabili coinvolte nella combinazione;
- Il kernel è formalmente calcolato come:
  $$
  k_\lor^d(\mathbf{x},\mathbf{z})=\binom{n}{d}-\binom{n-\langle\mathbf{x},\mathbf{x}\rangle}{d}-\binom{n-\langle\mathbf{z},\mathbf{z}\rangle}{d}+\binom{n-\langle\mathbf{x},\mathbf{x}\rangle-\langle\mathbf{z},\mathbf{z}\rangle+\langle\mathbf{x},\mathbf{z}\rangle}{d}
  $$

### Kernel in forma normale

Sia il C-kernel e il D-kernel sono definiti come funzioni di prodotti tra i vettori in input.
Possiamo sfruttare il kernel trick per costruire kernel booleani complessi per composizione.

- DNF-kernel: il feature space è formato da $d$ kernel disgiuntivi normalizzati di arità $c$
- CNF-kernel: il feature space è formato da $c$ kernel disgiuntivi normalizzati di arità $d$

### Apprendimento per kernel

- Possiamo definire kernel parametrici;
- Estrazione di feature trasduttive tramite kernel non lineari;
- Apprendimento di kernel spettrali;
- Apprendimento di kernel multipli.

### Metodi parametrici

Ottimizzazione dei parametri di una funzione kernel:
$$
k(\mathbf{x},\mathbf{z})=e^{-(\mathbf{x}-\mathbf{z})^T\mathbf{M}(\mathbf{x}-\mathbf{z})}
$$
Dove $\mathbf{M}$ è la $\beta_0\mathbf{I}$ nel caso di RBF e $\text{diag}(\beta_1,\ldots,\beta_m)$ per il caso anisotropico.

### Estrazioni di feature tramite kernel non lineari

Tramite l'analisi delle componenti principali su kernel (KPCA), una generalizzazione della PCA.
Il trucco è che gli autovettori sulle componenti principali nel feature space per calcolare implicitamente usando il kernel trick.

**Problema**: questo è un approccio trasduttivo poiché funziona su matrici kernel e non può essere immediatamente generalizzato su nuovi dati.

**Soluzione**: possono essere utilizzate tecniche basate su selezione degli esempi per approssimare i valori del kernel sui nuovi esempi.

### Apprendimento di kernel spettrali

Le matrici kernel possono sempre essere scritte come:
$$
\mathbf{K}=\sum_{s=1}^n\lambda_s\mathbf{u}_s\mathbf{u}_s^T
$$
dove $\lambda_s$ e $\mathbf{u}_s$ sono gli autovalori e i corrispettivi autovettori della matrice $\mathbf{K}$:

- utilizzando la trasformazione $\tau(\lambda)$ possiamo lavorare implicitamente sulla mappatura;
- l'idea è di ottimizzare lo spettro della matrice kernel. Questo è anche un approccio trasduttivo, dunque soffre degli stessi problemi dei metodi precedenti.

## Clustering

Il clustering è il processo di raggruppamento di un insieme di oggetti in gruppi di oggetti simili.
È la forma più comune di apprendimento non supervisionato.
Al contrario dell'apprendimento supervisionato, nell'apprendimento non supervisionato non disponiamo di una classificazione degli esempi.

Dati:

- un insieme di esempi $D=\{d_1,\ldots,d_n\}$ con $d_i\in\mathcal{D}$;
- una misura di similarità;
- un numero desiderato di cluster $K$;

calcolare:

- una funzione di assegnamento $\gamma:\mathcal{D}\rightarrow\{1,\ldots,K\}$ tale che nessun cluster sia vuoto.

### Scelte da fare nel Clustering

- Scelta della rappresentazione
  - Vector space
  - Normalizzazione
  - Kernel
  - Nozione di similarità/distanza
- Quanti cluster
  - Numero fissato a priori
  - Completamente guidato dai dati
  - Evitare cluster banali, troppo grandi o troppo piccoli

### Funzione obbiettivo

Spesso, il goal di un algoritmo di clustering è quello di ottimizzare una funzione obbiettivo; in questi casi, il clustering è un problema di ricerca (ottimizzazione).

Ci sono $K^n/K!$ clustering diversi possibili.

La maggior parte degli algoritmi di partizionamento partono da un assegnamento (partizione) per poi raffinarlo.

Avere molti minimi locali nella funzione obbiettivo implica che punti di partenza diversi possono portare a partizioni finali molto diverse (e non ottimali).

### Valutazione di un clustering

**Criteri interni**: dipendono dalla nozione di similarità e/o dalla rappresentazione scelta.
Andiamo a valutare la similarità intra-classe e inter-classe

**Criteri esterni**: dato un ground truth (una verità nota) data dall'esterno misurano la sua vicinanza al clustering prodotto.

#### Metodi interni

Un clustering è buono quando produce cluster nei quali la similarità intra-classe è alta e la similarità inter-classe è bassa.

Notare che tale misura di qualità dipende fortemente dalla rappresentazione scelta e dalla misura di similarità impiegata per calcolarla.

Ad esempio:
$$
V(D,\gamma)=\sum_{k=1}^K\sum_{i:\gamma(d_i)=k}\parallel\mathbf{x}_i-c_k\parallel^2
$$
dove $c_k$ è il centroide del k-esimo cluster, ovvero la media degli esempi assegnati al cluster k-esimo.

#### Metodi esterni

La qualità è misurata come l'abilità dimostrata nel riconoscere alcuni o tutti i pattern nascosti e/o le classi latenti nei dati.

Disponiamo di una classificazione (ground truth) dei dati che abbiamo clusterizzato e che non abbiamo usato per fare apprendimento e vogliamo misurare quanto il clustering prodotto assomigli a tale ground truth.

Assumiamo esempi di $C$ classi diverse clusterizzati su $K$ cluster $\omega_1,\ldots,\omega_K$

- **Purity**, ovvero il ratio tra il numero di elementi della classe dominante in un cluster e la cardinalità del cluster;
- **RandIndex**, simile alla nozione di accuratezza (accuracy) usata nella classificazione, considerando per ogni coppia di esempi se essi sono stati distribuiti correttamente nei cluster;
- Altri metodi come l'entropia o la mutua informazione tra classi del ground truth e i cluster prodotti.

### Algoritmi di clustering

- Algoritmi di partizionamento
  - Di solito partono con una partizione random parziale.
  - Iterativamente si cerca di raffinare tale partizione.
  - Un esempio di questo tipo di algoritmi sono è K-means

- Algoritmi gerarchici
  - Bottom-up o agglomerativi
  - Top-down o divisivi

### Algoritmo K-Means

Si assume che gli esempi siano vettori a valori reali

Per ogni cluster minimizziamo la media della distanza tra gli esempi e il centro del cluster, il centroide:
$$
\mu(c)=\frac{1}{|c|}\sum_{x\in c}x
$$
Le istanze vengono assegnate ai cluster in base alla similarità/distanza degli esempi dai centroidi dei cluster correnti.

1. Genera $K$ punti nello spazio (seed). Questi punti rappresentano i centroidi iniziali;
1. Assegna ogni esempio al cluster il cui centroide è più vicino;
1. Dopo aver assegnato tutti gli esempi ricalcola la posizione dei $K$ centroidi;
1. Ripeter i passi 2. e 3. fino a quando i centroidi si stabilizzano.

### Algoritmi gerarchici

Il numero di cluster $K$ ottimale non è dato di solito. Gli algoritmi gerarchici sono una buona alternativa in questi casi.

Costruiamo una tassonomia gerarchica (tree-based) da un insieme di esempi che rappresenta la struttura dei cluster (dendogramma).

Un primo approccio è considerare l'applicazione ricorsiva di un algoritmo di clustering partizionale (algoritmo gerarchico divisivo).

L'asse $y$ del dendogramma rappresenta la similarità della combinazione, ovvero la similarità tra i cluster che vengono fusi.

Si assume che l'operazione di merge sia monotona, ovvero se $s_1,\ldots,s_k$ sono successive similarità ottenute nella fusione, allora $s_1>s_2>\ldots>s_k$.

Il clustering è ottenuto tagliando il dendogramma dal livello desiderato: ogni componente connessa è un cluster.

#### Clustering Gerarchico Agglomerativo (HAC)

Partiamo con cluster separato per ogni esempio, poi fondiamo via via le coppie di cluster più vicine, fino ad ottenere un solo cluster.

La storia delle fusioni forma un albero binario gerarchico.

Quale metrica scegliamo per definire la coppia di cluster più vicina?

| Metrica       | Definizione                                   | Complessità    | Problematiche          |
| ------------- | --------------------------------------------- | -------------- | ---------------------- |
| Single-link   | Massima similitudine tra due punti qualsiasi  | $O(N^2)$       | Chaining effect        |
| Complete-link | minima similitudine tra due punti qualsiasi   | $O(N^2log(N))$ | Sensibile agli outlier |
| Centroid      | Similarità tra i centroidi                    | $O(N^2log(N))$ | Non monotona           |
| Group-average | Similarità media tra ciascuna coppia di punti | $O(N^2log(N))$ | Ok                     |

Con "non monotona" si intende il problema che all'aggiornamento della posizione dei centroidi è possibile che il la misura di similarità peggiori.

## Sistemi di raccomandazione

### Tipologie di feedback in RS

- **Feedback esplicito**
  - Rate degli item su una scala ordinata (stellette);
  - Un ordine degli item in base alla preferenza;
  - Preferenze su coppie di item.

- **Feedback implicito**:
  - Elenco degli item che l'utente ha visionato/comperato in passato;
  - Analisi dei tempi di permanenza di un utente in un sito;
  - Rete sociale di un utente

### Approcci in RS

- **Content Based (CB)**
  - Raccomando gli item più simili a quelli per cui l'utente ha già mostrato interesse.
- **Collaborative Filtering (CF)**
  - Raccomando a un utente gli item più simili a quelli che piacciono ai suoi simili o, viceversa, gli item più simili a quelli che piacciono a lui.
  - Similarità item-item: due oggetti sono simili se tendono ad ottenere lo stesso rate dagli utenti.
  - Similarità user-user: due utenti sono simili se tendono a dare lo stesso rate agli oggetti.
  - In questo caso non si usa conoscenza specifica su oggetti e utenti ma solo caratteristiche del comportamento sociale determinato dall'interazione utenti-item, i rating.
- **Metodi ibridi**
  - CB > CF quando ho poco storico (cold-start problem);
  - CF > CB quando l'informazione implicita contenuta sull'interazione diventa prevalente rispetto a quella esplicita del contenuto;
  - In casi intermedi possiamo utilizzare metodi ibridi che combinano entrambi gli approcci.

### Matrice di rating per feedback esplicito

La matrice di Rating rappresenta per ogni coppia utente/item il gradimento dell'item per l'utente.
È una matrice molto sparsa, tipicamente solo lo 0.1% dei valori sono presenti.

Si nota l'effetto long-tail, in cui pochi articoli/clienti hanno molti valori inseriti nella matrice, mentre la frequenza scende molto velocemente e resta basso fino ad azzerarsi.

### Problemi in RS

- **Rate prediction**: predizione di un'entry mancante dalla matrice di rating
- **Item Top-N reccomendation**: predizione degli $N$ item item di maggior gradimento per un dato user.

In entrambi i casi si utilizza Matrix Factorization per apprendere una rappresentazione per gli utenti e per gli item in modo che il prodotto scalare approssimare i rate.

Il problema nel secondo caso è come trattare i dati mancanti.

### Valutazione del rating

#### Rates Prediction (Root Mean Squared Error)

Sia $\hat{r}_{ui}$ la predizione del rate data dal mio predittore e $r_{ui}$ la reale valutazione data dall''item $i$ dall'utente $u$
$$
RMSE=\sqrt{\frac{1}{|Te|}\sum_{(u,i)\in Te}(r_{ui}-\hat{r}_{ui})^2}
$$

#### Top-N Reccomendation

Si usano metriche adatte per il ranking, infatti bisogna valutare quanto gli item effettivamente rilevanti per un utente vengano messi in alto nel ranking prodotto dal predittore:

- AUC (Area Under ROC Curve): dove in pratica si calcola il numero di coppie di item correttamente ordinate;
- Alternative più precision-oriented: prec@n, MAP, NDCG, ecc.

### Collaborative Filtering

#### Matrix Factorizzation e Regressione

Cerchiamo vettori $\mathbf{x}_u$ e $\mathbf{y}_i$ per ogni utente e per ogni item, tale che $\hat{r}_{ui}=\mathbf{x}_u^T\mathbf{y}_i$, e
$$
\text{min}_{\mathbf{x}_u}\sum_{i\in R(u)}|r_{ui}-\mathbf{x}_u^T\mathbf{y}_i|^2+\beta_u\parallel\mathbf{x}_u\parallel^2
$$
$$
\text{max}_{\mathbf{y}_i}\sum_{u\in R(i)}|r_{ui}-\mathbf{x}_u^T\mathbf{y}_i|^2+\beta_u\parallel\mathbf{x}_u\parallel^2
$$

#### Nearest Neighbour Based

$$
\hat{r}_{ui}=\frac{\sum_v\in R(i)\mathbf{w}_{uv}r_{vi}}{\sum_v\in R(i)\mathbf{w}_{uv}}
$$
$$
\hat{r}_{ui}=\frac{\sum_j\in R(u)\mathbf{w}_{ij}r_{uj}}{\sum_j\in R(u)\mathbf{w}_{ij}}
$$

$$
\mathbf{w}_{uv}=\frac
{|I(u)\cap I(v)|}
{|I(u)|^\frac{1}{2}|I(v)|^\frac{1}{2}}
$$
$$
\mathbf{w}_{ij}=\frac
{|U(i)\cap U(j)|}
{|U(i)|^\frac{1}{2}|U(j)|^\frac{1}{2}}
$$
Dove $I(u)$ è l'insieme degli item ratati dall'utente $u$ e $U(i)$ è l'insieme di utenti che hanno ratato l'item $i$.
